﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using KompasLibrary.Model.Exceptions;
using KompasLibrary.Model.Interfaces;
using KompasLibrary.Model;
using KompasLibrary.Model.Types;
using KompasLibrary.KompasAPI;
using System.Drawing;

namespace KompasLibrary.Controller
{  
    /// <summary>
    /// Контроллер представления - class GUI
    /// </summary>
    public class ControllerGUI
    {
    #region Поля
        /// <summary>
        /// Объект представления
        /// </summary>
        private GUI _view;

        /// <summary>
        /// Объект модели
        /// </summary>
        private ITubeModel _model;

        /// <summary>
        /// Список шагов мастера настройки модели
        /// </summary>
        private StepList _steps;

        /// <summary>
        /// Объект обертки для представления
        /// </summary>
        private WinWrapper _wrapper;

    #endregion

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="model">Ссылка на объект модели</param>
        /// <param name="kompasAPI">Ссылка на объект компасовской обертки</param>
        public ControllerGUI(ITubeModel model, KompasWrapper kompasAPI)
        {
            _model = model;            

            _view = new GUI(this, _model);
            _steps = new StepList();            

            List<Panel> stepPanels = _view.StepPanels;
            _steps.Add(new Step(stepPanels[0], "Шаг 1. Выберите тип трубы и ключевого упора"));
            _steps.Add(new Step(stepPanels[1], "Шаг 2. Выберите тип резьбы для каждого конца"));
            _steps.Add(new Step(stepPanels[2], "Шаг 3. Настройте параметры трубы"));   
            _steps.Add(new Step(stepPanels[3], "Шаг 4. Настройте параметры ключевого упора"));
            _steps.Add(new Step(stepPanels[4], "Шаг 5. Подтвердите настроенные параметры"));

            if(kompasAPI != null)
                _wrapper = new WinWrapper((IntPtr)kompasAPI.KompasObj.ksGetHWindow());
        }

    #region Управление контроллером 

        /// <summary>
        /// Получить интерфейс представления
        /// </summary>
        /// <param name="view">Объект представления</param>
        public IView View
        {
            get { return _view; }
        }

        /// <summary>
        /// Начало работы
        /// </summary>
        public void Start()
        {
            _model.Initialize();
            _steps.ShowCurrent();
            _view.ShowDialog(_wrapper);            
        }

        /// <summary>
        /// Окончание работы
        /// </summary>
        public void Stop()
        {
            _model.Dispose();
            _view.Close();
            _view.Dispose();
        }
    #endregion

    #region Управление навигацией

        /// <summary>
        /// Переход к следующему шагу мастера 
        /// </summary>
        public void Next()
        {
            if (_steps.IsCurrentLast())
            {
                _view.Hide();
                _model.Create();
                //_view.Close();
                //_model.Dispose();                
            }
            else
            {
                if(CanSendToModelDataFromStep(_steps.NumberOfCurrent))
                {
                    _steps.TurnToNext();
                    _steps.ShowCurrent();
                    if (_steps.IsCurrentLast())
                        _view.NextBtnCapture = "Построить";
                    _view.StepTitle = _steps.TitleOfCurrent;
                    _view.IsPrevButtonEnabled = true;
                }
            }
        }

        /// <summary>
        /// Проверка возможности передачи в модель данных с определенного шага
        /// </summary>
        /// <param name="stepNo">Номер шага</param>
        /// <returns>Вывод о валидности данных на шаге</returns>
        private bool CanSendToModelDataFromStep(int stepNo)
        {
            ITubeParameters modelParams = _model.Parameters;
            switch (stepNo)
            {
                case 3:
                    try
                    {
                        modelParams.Diameter = _view.DiameterTxtValue;
                        modelParams.Length = _view.LengthTxtValue;
                        modelParams.ChannelDiameter = _view.ChanDiameterTxtValue;
                        modelParams.WallThickness = _view.WallThicknessTxtValue;
                        if (modelParams.IsNeedStair)
                        {
                            modelParams.StairDiameter = _view.StairDiameterTxtBoxValue;
                            modelParams.StairLength = _view.StairLengthTxtBoxValue;
                        }
                    }
                    catch (WrongInputException ex)
                    {
                        NameOfTubeParameter paramName = (NameOfTubeParameter)ex.Data["Parameter"];
                        _view.MarkBoxWithErrorParameter(paramName);
                        return false;
                    }
                    break;
                case 4:
                    try
                    {
                        modelParams.KeySize = _view.KeySizeTxtValue;
                        modelParams.KeyLength = _view.KeyLengthTxtValue;
                        modelParams.KeyOffset = _view.KeyOffsetTxtValue;
                    }
                    catch (WrongInputException ex)
                    {
                        NameOfTubeParameter paramName = (NameOfTubeParameter)ex.Data["Parameter"];
                        _view.MarkBoxWithErrorParameter(paramName);
                        return false;
                    }
                    break;
            }
            return true;
        }

        /// <summary>
        /// Переход к пердыдущему шагу мастера
        /// </summary>
        public void Prev()
        {
            _steps.TurnToPrev();
            _steps.ShowCurrent();
            if (_steps.IsCurrentFirst())
                _view.IsPrevButtonEnabled = false;
            _view.StepTitle = _steps.TitleOfCurrent;
            _view.NextBtnCapture = "Далее";
            
        }
    #endregion

        /// <summary>
        /// Действия при изменении пользователем определенного параметра модели
        /// </summary>
        /// <param name="name">Название параметра модели</param>
        public void ChangedParameter(NameOfTubeParameter name)
        {
            ITubeParameters modelParams = _model.Parameters;
            try
            {
                switch (name)
                {
                    case NameOfTubeParameter.TypeOfTube:
                        if (_view.IsTubeTypeAChecked)
                        {
                            modelParams.TypeOfTube = TubeType.Standart;
                            _view.TubeTypeLabel = "Стандартная труба";
                            _view.IsConnTypeGroupBoxVisible = false;
                            _view.TubePicture = Properties.Resources.typeA;
                        }
                        else if (_view.IsTubeTypeBChecked && _view.IsConnTypeMChecked)
                        {
                            modelParams.TypeOfTube = TubeType.ConnectorMuf;
                            _view.TubeTypeLabel = "Переводник муфтовый";
                            _view.IsConnTypeGroupBoxVisible = true;
                            _view.TubePicture = Properties.Resources.typeB;
                        }
                        else if (_view.IsTubeTypeBChecked && _view.IsConnTypeNChecked)
                        {
                            modelParams.TypeOfTube = TubeType.ConnectorNip;
                            _view.TubeTypeLabel = "Переводник ниппельный";
                            _view.IsConnTypeGroupBoxVisible = true;
                            _view.TubePicture = Properties.Resources.typeB1;
                        }
                        break;
                    case NameOfTubeParameter.TypeOfKey:
                        if (_view.IsKeyTypeRoundChecked)
                        {
                            modelParams.TypeOfKey = KeyType.Round;
                            _view.KeyTypeLabel = "Круговой";
                            _view.KeyPicture = Properties.Resources.keyTypeRound;
                        }
                        else if (_view.IsKeyTypeHexChecked)
                        {
                            modelParams.TypeOfKey = KeyType.Hexahedron;
                            _view.KeyTypeLabel = "Шестигранник";
                            _view.KeyPicture = Properties.Resources.keyTypeHex;
                        }
                        break;
                    case NameOfTubeParameter.LeftSideThread:
                        modelParams.LeftSideThreadName = _view.LeftThreadComboBoxCurrent;
                        break;
                    case NameOfTubeParameter.RightSideThread:
                        modelParams.RightSideThreadName = _view.RightThreadComboBoxCurrent;
                        break;
                    case NameOfTubeParameter.Diameter:
                        modelParams.Diameter = _view.DiameterTxtValue;
                        break;
                    case NameOfTubeParameter.Length:
                        modelParams.Length = _view.LengthTxtValue;
                        break;
                    case NameOfTubeParameter.ChannelDiameter:
                        modelParams.ChannelDiameter = _view.ChanDiameterTxtValue;
                        break;
                    case NameOfTubeParameter.WallThickness:
                        modelParams.WallThickness = _view.WallThicknessTxtValue;
                        break;
                    case NameOfTubeParameter.StairDiameter:
                        modelParams.StairDiameter = _view.StairDiameterTxtBoxValue;
                        break;
                    case NameOfTubeParameter.StairLength:
                        modelParams.StairLength = _view.StairLengthTxtBoxValue;
                        break;
                    case NameOfTubeParameter.KeyLength:
                        modelParams.KeyLength = _view.KeyLengthTxtValue;
                        break;
                    case NameOfTubeParameter.KeySize:
                        modelParams.KeySize = _view.KeySizeTxtValue;
                        break;
                    case NameOfTubeParameter.KeyOffset:
                        modelParams.KeyOffset = _view.KeyOffsetTxtValue;
                        break;
                }
                _view.HasInputError = false;
            }
            catch (WrongInputException)
            {
                _view.HasInputError = true;
            }
        }

        /// <summary>
        /// Установить подсветку параметра на картинке
        /// </summary>
        /// <param name="param">Название параметра модели</param>
        public void SetHighlightingOfParameter(NameOfTubeParameter param)
        {
            HighlightParameterWithColor(param, Color.Red);
        }

        /// <summary>
        /// Убрать подсветку параметра на картинке
        /// </summary>
        /// <param name="param">Название параметра модели</param>
        public void RefuseHighlightingOfParameter(NameOfTubeParameter param)
        {
            HighlightParameterWithColor(param, Color.LightGray);
        }

        /// <summary>
        /// Создание подсветки параметра определенного цвета на картинке
        /// </summary>
        /// <param name="param">Название параметра модели</param>
        /// <param name="col">Цвет посветки</param>
        private void HighlightParameterWithColor(NameOfTubeParameter param, Color col)
        {
            ITubeParameters parameters = _model.Parameters;
            switch (param)
            {
                case NameOfTubeParameter.Diameter:
                    switch (parameters.TypeOfTube)
                    {
                        case TubeType.Standart:
                            _view.MainSizesPicture = DrawLineOnPicture(_view.MainSizesPicture, new Point(25, 43), new Point(25, 162), col);                            
                            break;
                        case TubeType.ConnectorMuf:
                            _view.MainSizesPicture = DrawLineOnPicture(_view.MainSizesPicture, new Point(25, 38), new Point(25, 155), col);
                            break;
                        case TubeType.ConnectorNip:
                            _view.MainSizesPicture = DrawLineOnPicture(_view.MainSizesPicture, new Point(16, 48), new Point(16, 128), col);
                            break;
                    }
                    break;
                case NameOfTubeParameter.Length:
                    switch (parameters.TypeOfTube)
                    {
                        case TubeType.Standart:
                            _view.MainSizesPicture = DrawLineOnPicture(_view.MainSizesPicture, new Point(60, 17), new Point(261, 17), col);
                            break;
                        case TubeType.ConnectorMuf:
                            _view.MainSizesPicture = DrawLineOnPicture(_view.MainSizesPicture, new Point(70, 16), new Point(264, 16), col);
                            break;
                        case TubeType.ConnectorNip:
                            _view.MainSizesPicture = DrawLineOnPicture(_view.MainSizesPicture, new Point(63, 37), new Point(274, 37), col);
                            break;
                    }
                    break;
                case NameOfTubeParameter.ChannelDiameter:
                    switch (parameters.TypeOfTube)
                    {
                        case TubeType.Standart:
                            _view.MainSizesPicture = DrawLineOnPicture(_view.MainSizesPicture, new Point(50, 90), new Point(50, 116), col);
                            break;
                        case TubeType.ConnectorMuf:
                            _view.MainSizesPicture = DrawLineOnPicture(_view.MainSizesPicture, new Point(55, 74), new Point(55, 111), col);
                            break;
                        case TubeType.ConnectorNip:
                            _view.MainSizesPicture = DrawLineOnPicture(_view.MainSizesPicture, new Point(46, 76), new Point(46, 97), col);
                            break;
                    }
                    break;
                case NameOfTubeParameter.WallThickness:
                    switch (parameters.TypeOfTube)
                    {
                        case TubeType.Standart:
                            _view.MainSizesPicture =  DrawLineOnPicture(_view.MainSizesPicture, new Point(50, 118), new Point(50, 162), col);
                            break;
                        case TubeType.ConnectorMuf:
                            _view.MainSizesPicture = DrawLineOnPicture(_view.MainSizesPicture, new Point(55, 113), new Point(55, 154), col);
                            break;
                        case TubeType.ConnectorNip:
                            _view.MainSizesPicture = DrawLineOnPicture(_view.MainSizesPicture, new Point(46, 99), new Point(46, 128), col);
                            break;
                    }
                    break;
                case NameOfTubeParameter.StairDiameter:
                    _view.MainSizesPicture = DrawLineOnPicture(_view.MainSizesPicture, new Point(37, 34), new Point(37, 151), col);
                    break;
                case NameOfTubeParameter.StairLength:
                    _view.MainSizesPicture = DrawLineOnPicture(_view.MainSizesPicture, new Point(67, 14), new Point(176, 14), col);
                    break;
                case NameOfTubeParameter.KeySize:
                    switch (parameters.TypeOfKey)
                    {
                        case KeyType.Round:
                            _view.KeySetsPicture = DrawLineOnPicture(_view.KeySetsPicture, new Point(298, 62), new Point(298, 145), col);
                            break;
                        case KeyType.Hexahedron:
                            _view.KeySetsPicture = DrawLineOnPicture(_view.KeySetsPicture, new Point(304, 47), new Point(304, 137), col);
                            break;
                    } 
                    break;
                case NameOfTubeParameter.KeyOffset:
                    switch (parameters.TypeOfKey)
                    {
                        case KeyType.Round:
                            _view.KeySetsPicture = DrawLineOnPicture(_view.KeySetsPicture, new Point(67, 44), new Point(155, 44), col);
                            break;
                        case KeyType.Hexahedron:
                            _view.KeySetsPicture = DrawLineOnPicture(_view.KeySetsPicture, new Point(96, 12), new Point(158, 12), col);
                            break;
                    } 
                    break;
                case NameOfTubeParameter.KeyLength:
                    switch (parameters.TypeOfKey)
                    {
                        case KeyType.Round:
                            _view.KeySetsPicture = DrawLineOnPicture(_view.KeySetsPicture, new Point(26, 44), new Point(64, 44), col);
                            break;
                        case KeyType.Hexahedron:
                            _view.KeySetsPicture = DrawLineOnPicture(_view.KeySetsPicture, new Point(16, 12), new Point(94, 12), col);
                            break;
                    } 
                    break;
            }
        }

        /// <summary>
        /// Рисование ортогональной линии на картинке
        /// </summary>
        /// <param name="pic">Изображение</param>
        /// <param name="init">Начальная точка линии</param>
        /// <param name="end">Конечная точка линии</param>
        /// <param name="col">Цвет линиии</param>
        /// <returns>Измененное изображение</returns>
        private Bitmap DrawLineOnPicture(Bitmap pic, Point init, Point end, Color col)
        {
            if (init.X == end.X)
            {
                for (int i = init.Y; i < end.Y; i++)
                    pic.SetPixel(init.X, i, col);
            }
            else if (init.Y == end.Y)
            {
                for (int i = init.X; i < end.X; i++)
                    pic.SetPixel(i, init.Y, col);
            }
            return pic;
        }
    }


}
