﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KompasLibrary.Controller
{
    /// <summary>
    /// Шаг мастера создания модели
    /// </summary>
    internal class Step
    {
        /// <summary>
        /// Объект панали с содержимым шага
        /// </summary>
        private Panel _content;

        /// <summary>
        /// Название шага
        /// </summary>
        private string _title;

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public Step()
        {
            _content = null;
            _title = "";
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="content">Панель для шага</param>
        /// <param name="title">Название шага</param>
        public Step(Panel content, string title)
        {
            _content = content;
            _title = title;
        }

        /// <summary>
        /// Свойство для доступа к объекту панели, только чтение
        /// </summary>
        public Panel Content
        {
            get { return _content; }
        }

        /// <summary>
        /// Свойство для доступа к имени панели, только чтение
        /// </summary>
        public string ContentName
        {
            get { return _content.Name; }
        }

        /// <summary>
        /// Свойство для доступа к заголовку шага, только чтение
        /// </summary>
        public string Title
        {
            get { return _title; }
        }

        /// <summary>
        /// Показать панель шага
        /// </summary>
        public void Show()
        {
            _content.Show();
        }

        /// <summary>
        /// Скрыть панель шага
        /// </summary>
        public void Hide()
        {
            _content.SendToBack();
        }
    }

    /// <summary>
    /// Список шагов мастера создания модели
    /// </summary>
    internal class StepList
    {
        /// <summary>
        /// Список объектов шагов
        /// </summary>
        private List<Step> _steps;

        /// <summary>
        /// Количество шагов
        /// </summary>
        private int _count;

        /// <summary>
        /// Идентификатор текущего шага
        /// </summary>
        private int _currId;

        /// <summary>
        /// Конструктор
        /// </summary>
        public StepList()
        {
            _steps = new List<Step>();
            _count = 0;
            _currId = 0;
        }

        /// <summary>
        /// Добавление нового шага в список
        /// </summary>
        /// <param name="item">Новый шаг</param>
        public void Add(Step item)
        {
            _steps.Add(item);
            _count++;
        }

        /// <summary>
        /// Очистка списка
        /// </summary>
        public void Clear()
        {
            _steps.Clear();
            _count = 0;
            _currId = 0;
        }

        /// <summary>
        /// Получить имя панели для текущего шага
        /// </summary>
        /// <returns>Имя панели текущего шага</returns>
        public string ContentNameOfCurrent()
        {
            Step currElem = _steps.ElementAt(_currId);
            return currElem.ContentName;
        }

        /// <summary>
        /// Свойство для доступа к заголовку текущего шага, только чтение
        /// </summary>
        public string TitleOfCurrent
        {
            get
            {
                Step currElem = _steps.ElementAt(_currId);
                return currElem.Title;
            }

        }

        /// <summary>
        /// Свойство для получения номера текущего шага, только чтение
        /// </summary>
        public int NumberOfCurrent
        {
            get { return _currId + 1; }
        }

        /// <summary>
        /// Показать текущий шаг
        /// </summary>
        public void ShowCurrent()
        {
            for (int i = 0; i < _count; i++)
            {
                if (i != _currId)
                {
                    Step elem = _steps.ElementAt(i);
                    elem.Hide();
                }
            }
            Step currElem = _steps.ElementAt(_currId);
            currElem.Show();
        }

        /// <summary>
        /// Проверка является ли текущий шаг первым
        /// </summary>
        /// <returns>Результат проверки - истина или ложь</returns>
        public bool IsCurrentFirst()
        {
            return (_currId == 0 && _count > 0);
        }

        /// <summary>
        /// Проверка является ли текущий шаг последним
        /// </summary>
        /// <returns>Результат проверки - истина или ложь</returns>
        public bool IsCurrentLast()
        {
            return (_currId == _count - 1);
        }

        /// <summary>
        /// Переход к следующему шагу
        /// </summary>
        public void TurnToNext()
        {
            if (++_currId >= _count)
                _currId = _count - 1;
        }

        /// <summary>
        /// Переход к предыдущему шагу
        /// </summary>
        public void TurnToPrev()
        {
            if (--_currId < 0)
                _currId = 0;
        }
    }
}
