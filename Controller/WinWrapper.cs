﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KompasLibrary.Controller
{
    /// <summary>
    /// Класс-обертка для отображение формы в окне с указанным дескриптором
    /// </summary>
    public class WinWrapper : IWin32Window
    {
        /// <summary>
        /// Дескриптор родительского окна
        /// </summary>
        private IntPtr _hwnd;

        /// <summary>
        /// Свойство для доступа к дескриптору
        /// </summary>
        public IntPtr Handle
        {
            get { return _hwnd; }
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="handle">Дескриптор родительского окна</param>
        public WinWrapper(IntPtr handle)
        {
            _hwnd = handle;
        }
    }
}
