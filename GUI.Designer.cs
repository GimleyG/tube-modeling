﻿namespace KompasLibrary
{
    partial class GUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.nextBtn = new System.Windows.Forms.Button();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.headPanel = new System.Windows.Forms.Panel();
            this.headerLabel = new System.Windows.Forms.Label();
            this.bottomPanel = new System.Windows.Forms.Panel();
            this.prevBtn = new System.Windows.Forms.Button();
            this.diameterLbl = new System.Windows.Forms.Label();
            this.diametrTxt = new System.Windows.Forms.TextBox();
            this.chanDiameterTxt = new System.Windows.Forms.TextBox();
            this.chanDiameterLbl = new System.Windows.Forms.Label();
            this.lengthLbl = new System.Windows.Forms.Label();
            this.lengthTxt = new System.Windows.Forms.TextBox();
            this.step1Panel = new System.Windows.Forms.Panel();
            this.connTypeGroupBox = new System.Windows.Forms.GroupBox();
            this.connTypeN = new System.Windows.Forms.RadioButton();
            this.connTypeM = new System.Windows.Forms.RadioButton();
            this.drawCaptionKTypeLabel = new System.Windows.Forms.Label();
            this.drawCaptionTTypeLabel = new System.Windows.Forms.Label();
            this.keyPicture = new System.Windows.Forms.PictureBox();
            this.keyTypeGroupBox = new System.Windows.Forms.GroupBox();
            this.keyTypeHex = new System.Windows.Forms.RadioButton();
            this.keyTypeRound = new System.Windows.Forms.RadioButton();
            this.tubeTypeGroupBox = new System.Windows.Forms.GroupBox();
            this.tubeTypeA = new System.Windows.Forms.RadioButton();
            this.tubeTypeB = new System.Windows.Forms.RadioButton();
            this.tubePicture = new System.Windows.Forms.PictureBox();
            this.step3Panel = new System.Windows.Forms.Panel();
            this.stairParamsBox = new System.Windows.Forms.GroupBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.stairLengthTxt = new System.Windows.Forms.TextBox();
            this.stairDiameterTxt = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.wallThicknessTxt = new System.Windows.Forms.TextBox();
            this.mainSizesPicture = new System.Windows.Forms.PictureBox();
            this.step2Panel = new System.Windows.Forms.Panel();
            this.cautionLabelStp2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.rightThreadComboBox = new System.Windows.Forms.ComboBox();
            this.leftThreadComboBox = new System.Windows.Forms.ComboBox();
            this.leftThreadPicture = new System.Windows.Forms.PictureBox();
            this.rightThreadPicture = new System.Windows.Forms.PictureBox();
            this.step4Panel = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.keySetsPicture = new System.Windows.Forms.PictureBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.keyOffsetTxt = new System.Windows.Forms.TextBox();
            this.keyLengthTxt = new System.Windows.Forms.TextBox();
            this.keySizeTxt = new System.Windows.Forms.TextBox();
            this.step5Panel = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.previewPicture = new System.Windows.Forms.PictureBox();
            this.settingsPreviewTxtBox = new System.Windows.Forms.RichTextBox();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.headPanel.SuspendLayout();
            this.bottomPanel.SuspendLayout();
            this.step1Panel.SuspendLayout();
            this.connTypeGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.keyPicture)).BeginInit();
            this.keyTypeGroupBox.SuspendLayout();
            this.tubeTypeGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tubePicture)).BeginInit();
            this.step3Panel.SuspendLayout();
            this.stairParamsBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainSizesPicture)).BeginInit();
            this.step2Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.leftThreadPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightThreadPicture)).BeginInit();
            this.step4Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.keySetsPicture)).BeginInit();
            this.step5Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.previewPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // nextBtn
            // 
            this.nextBtn.Location = new System.Drawing.Point(406, 16);
            this.nextBtn.Name = "nextBtn";
            this.nextBtn.Size = new System.Drawing.Size(75, 23);
            this.nextBtn.TabIndex = 0;
            this.nextBtn.Text = "Далее";
            this.nextBtn.UseVisualStyleBackColor = true;
            this.nextBtn.Click += new System.EventHandler(this.nextBtn_Click);
            // 
            // cancelBtn
            // 
            this.cancelBtn.Location = new System.Drawing.Point(527, 16);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.cancelBtn.TabIndex = 1;
            this.cancelBtn.Text = "Отменить";
            this.cancelBtn.UseVisualStyleBackColor = true;
            this.cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // headPanel
            // 
            this.headPanel.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.headPanel.Controls.Add(this.headerLabel);
            this.headPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.headPanel.Location = new System.Drawing.Point(0, 0);
            this.headPanel.Name = "headPanel";
            this.headPanel.Size = new System.Drawing.Size(614, 60);
            this.headPanel.TabIndex = 11;
            // 
            // headerLabel
            // 
            this.headerLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.headerLabel.AutoSize = true;
            this.headerLabel.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.headerLabel.Location = new System.Drawing.Point(125, 18);
            this.headerLabel.Name = "headerLabel";
            this.headerLabel.Size = new System.Drawing.Size(370, 23);
            this.headerLabel.TabIndex = 6;
            this.headerLabel.Text = "Шаг1: Выберите тип трубы и ключевого упора";
            // 
            // bottomPanel
            // 
            this.bottomPanel.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.bottomPanel.Controls.Add(this.prevBtn);
            this.bottomPanel.Controls.Add(this.nextBtn);
            this.bottomPanel.Controls.Add(this.cancelBtn);
            this.bottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottomPanel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.bottomPanel.Location = new System.Drawing.Point(0, 333);
            this.bottomPanel.Name = "bottomPanel";
            this.bottomPanel.Size = new System.Drawing.Size(614, 60);
            this.bottomPanel.TabIndex = 12;
            // 
            // prevBtn
            // 
            this.prevBtn.Enabled = false;
            this.prevBtn.Location = new System.Drawing.Point(325, 16);
            this.prevBtn.Name = "prevBtn";
            this.prevBtn.Size = new System.Drawing.Size(75, 23);
            this.prevBtn.TabIndex = 2;
            this.prevBtn.Text = "Назад";
            this.prevBtn.UseVisualStyleBackColor = true;
            this.prevBtn.Click += new System.EventHandler(this.prevBtn_Click);
            // 
            // diameterLbl
            // 
            this.diameterLbl.AutoSize = true;
            this.diameterLbl.Location = new System.Drawing.Point(115, 49);
            this.diameterLbl.Name = "diameterLbl";
            this.diameterLbl.Size = new System.Drawing.Size(53, 13);
            this.diameterLbl.TabIndex = 2;
            this.diameterLbl.Text = "Диаметр";
            // 
            // diametrTxt
            // 
            this.diametrTxt.BackColor = System.Drawing.SystemColors.Window;
            this.diametrTxt.Location = new System.Drawing.Point(178, 46);
            this.diametrTxt.MaxLength = 6;
            this.diametrTxt.Name = "diametrTxt";
            this.diametrTxt.Size = new System.Drawing.Size(49, 20);
            this.diametrTxt.TabIndex = 5;
            this.diametrTxt.Tag = "diametrTxt";
            this.diametrTxt.Enter += new System.EventHandler(this.txtStep3_Enter);
            this.diametrTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mainSetsTxt_KeyPress);
            this.diametrTxt.Leave += new System.EventHandler(this.txtStep3_Leave);
            this.diametrTxt.Validating += new System.ComponentModel.CancelEventHandler(this.inputTxt_Validating);
            this.diametrTxt.Validated += new System.EventHandler(this.inputTxt_Validated);
            // 
            // chanDiameterTxt
            // 
            this.chanDiameterTxt.Location = new System.Drawing.Point(178, 98);
            this.chanDiameterTxt.MaxLength = 6;
            this.chanDiameterTxt.Name = "chanDiameterTxt";
            this.chanDiameterTxt.Size = new System.Drawing.Size(49, 20);
            this.chanDiameterTxt.TabIndex = 7;
            this.chanDiameterTxt.Tag = "chanDiameterTxt";
            this.chanDiameterTxt.Enter += new System.EventHandler(this.txtStep3_Enter);
            this.chanDiameterTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mainSetsTxt_KeyPress);
            this.chanDiameterTxt.Leave += new System.EventHandler(this.txtStep3_Leave);
            this.chanDiameterTxt.Validating += new System.ComponentModel.CancelEventHandler(this.inputTxt_Validating);
            this.chanDiameterTxt.Validated += new System.EventHandler(this.inputTxt_Validated);
            // 
            // chanDiameterLbl
            // 
            this.chanDiameterLbl.AutoSize = true;
            this.chanDiameterLbl.Location = new System.Drawing.Point(78, 101);
            this.chanDiameterLbl.Name = "chanDiameterLbl";
            this.chanDiameterLbl.Size = new System.Drawing.Size(92, 13);
            this.chanDiameterLbl.TabIndex = 4;
            this.chanDiameterLbl.Text = "Диаметр канала";
            // 
            // lengthLbl
            // 
            this.lengthLbl.AutoSize = true;
            this.lengthLbl.Location = new System.Drawing.Point(115, 75);
            this.lengthLbl.Name = "lengthLbl";
            this.lengthLbl.Size = new System.Drawing.Size(40, 13);
            this.lengthLbl.TabIndex = 3;
            this.lengthLbl.Text = "Длина";
            // 
            // lengthTxt
            // 
            this.lengthTxt.Location = new System.Drawing.Point(178, 72);
            this.lengthTxt.MaxLength = 6;
            this.lengthTxt.Name = "lengthTxt";
            this.lengthTxt.Size = new System.Drawing.Size(49, 20);
            this.lengthTxt.TabIndex = 6;
            this.lengthTxt.Tag = "lengthTxt";
            this.lengthTxt.Enter += new System.EventHandler(this.txtStep3_Enter);
            this.lengthTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mainSetsTxt_KeyPress);
            this.lengthTxt.Leave += new System.EventHandler(this.txtStep3_Leave);
            this.lengthTxt.Validating += new System.ComponentModel.CancelEventHandler(this.inputTxt_Validating);
            this.lengthTxt.Validated += new System.EventHandler(this.inputTxt_Validated);
            // 
            // step1Panel
            // 
            this.step1Panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.step1Panel.Controls.Add(this.connTypeGroupBox);
            this.step1Panel.Controls.Add(this.drawCaptionKTypeLabel);
            this.step1Panel.Controls.Add(this.drawCaptionTTypeLabel);
            this.step1Panel.Controls.Add(this.keyPicture);
            this.step1Panel.Controls.Add(this.keyTypeGroupBox);
            this.step1Panel.Controls.Add(this.tubeTypeGroupBox);
            this.step1Panel.Controls.Add(this.tubePicture);
            this.step1Panel.Location = new System.Drawing.Point(0, 60);
            this.step1Panel.Name = "step1Panel";
            this.step1Panel.Size = new System.Drawing.Size(614, 270);
            this.step1Panel.TabIndex = 13;
            // 
            // connTypeGroupBox
            // 
            this.connTypeGroupBox.Controls.Add(this.connTypeN);
            this.connTypeGroupBox.Controls.Add(this.connTypeM);
            this.connTypeGroupBox.Location = new System.Drawing.Point(14, 189);
            this.connTypeGroupBox.Name = "connTypeGroupBox";
            this.connTypeGroupBox.Size = new System.Drawing.Size(158, 74);
            this.connTypeGroupBox.TabIndex = 8;
            this.connTypeGroupBox.TabStop = false;
            this.connTypeGroupBox.Text = "Тип переводника";
            this.connTypeGroupBox.Visible = false;
            // 
            // connTypeN
            // 
            this.connTypeN.AutoSize = true;
            this.connTypeN.Location = new System.Drawing.Point(32, 46);
            this.connTypeN.Name = "connTypeN";
            this.connTypeN.Size = new System.Drawing.Size(89, 17);
            this.connTypeN.TabIndex = 1;
            this.connTypeN.Tag = "connTypeN";
            this.connTypeN.Text = "Ниппельный";
            this.connTypeN.UseVisualStyleBackColor = true;
            this.connTypeN.CheckedChanged += new System.EventHandler(this.Step1_CheckedChanged);
            // 
            // connTypeM
            // 
            this.connTypeM.AutoSize = true;
            this.connTypeM.Checked = true;
            this.connTypeM.Location = new System.Drawing.Point(32, 23);
            this.connTypeM.Name = "connTypeM";
            this.connTypeM.Size = new System.Drawing.Size(78, 17);
            this.connTypeM.TabIndex = 0;
            this.connTypeM.TabStop = true;
            this.connTypeM.Tag = "connTypeM";
            this.connTypeM.Text = "Муфтовый";
            this.connTypeM.UseVisualStyleBackColor = true;
            this.connTypeM.CheckedChanged += new System.EventHandler(this.Step1_CheckedChanged);
            // 
            // drawCaptionKTypeLabel
            // 
            this.drawCaptionKTypeLabel.AutoSize = true;
            this.drawCaptionKTypeLabel.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.drawCaptionKTypeLabel.Location = new System.Drawing.Point(498, 178);
            this.drawCaptionKTypeLabel.Name = "drawCaptionKTypeLabel";
            this.drawCaptionKTypeLabel.Size = new System.Drawing.Size(67, 18);
            this.drawCaptionKTypeLabel.TabIndex = 7;
            this.drawCaptionKTypeLabel.Text = "Круговой";
            // 
            // drawCaptionTTypeLabel
            // 
            this.drawCaptionTTypeLabel.AutoSize = true;
            this.drawCaptionTTypeLabel.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.drawCaptionTTypeLabel.Location = new System.Drawing.Point(260, 178);
            this.drawCaptionTTypeLabel.Name = "drawCaptionTTypeLabel";
            this.drawCaptionTTypeLabel.Size = new System.Drawing.Size(123, 18);
            this.drawCaptionTTypeLabel.TabIndex = 6;
            this.drawCaptionTTypeLabel.Text = "Стандартная труба";
            // 
            // keyPicture
            // 
            this.keyPicture.Image = global::KompasLibrary.Properties.Resources.keyTypeRound;
            this.keyPicture.Location = new System.Drawing.Point(467, 31);
            this.keyPicture.Name = "keyPicture";
            this.keyPicture.Size = new System.Drawing.Size(134, 130);
            this.keyPicture.TabIndex = 5;
            this.keyPicture.TabStop = false;
            // 
            // keyTypeGroupBox
            // 
            this.keyTypeGroupBox.Controls.Add(this.keyTypeHex);
            this.keyTypeGroupBox.Controls.Add(this.keyTypeRound);
            this.keyTypeGroupBox.Location = new System.Drawing.Point(14, 98);
            this.keyTypeGroupBox.Name = "keyTypeGroupBox";
            this.keyTypeGroupBox.Size = new System.Drawing.Size(158, 74);
            this.keyTypeGroupBox.TabIndex = 4;
            this.keyTypeGroupBox.TabStop = false;
            this.keyTypeGroupBox.Text = "Тип ключевого упора";
            // 
            // keyTypeHex
            // 
            this.keyTypeHex.AutoSize = true;
            this.keyTypeHex.Location = new System.Drawing.Point(32, 46);
            this.keyTypeHex.Name = "keyTypeHex";
            this.keyTypeHex.Size = new System.Drawing.Size(98, 17);
            this.keyTypeHex.TabIndex = 1;
            this.keyTypeHex.Tag = "keyTypeHex";
            this.keyTypeHex.Text = "Шестигранник";
            this.keyTypeHex.UseVisualStyleBackColor = true;
            this.keyTypeHex.CheckedChanged += new System.EventHandler(this.Step1_CheckedChanged);
            // 
            // keyTypeRound
            // 
            this.keyTypeRound.AutoSize = true;
            this.keyTypeRound.Checked = true;
            this.keyTypeRound.Location = new System.Drawing.Point(32, 23);
            this.keyTypeRound.Name = "keyTypeRound";
            this.keyTypeRound.Size = new System.Drawing.Size(72, 17);
            this.keyTypeRound.TabIndex = 0;
            this.keyTypeRound.TabStop = true;
            this.keyTypeRound.Tag = "keyTypeRound";
            this.keyTypeRound.Text = "Круговой";
            this.keyTypeRound.UseVisualStyleBackColor = true;
            this.keyTypeRound.CheckedChanged += new System.EventHandler(this.Step1_CheckedChanged);
            // 
            // tubeTypeGroupBox
            // 
            this.tubeTypeGroupBox.Controls.Add(this.tubeTypeA);
            this.tubeTypeGroupBox.Controls.Add(this.tubeTypeB);
            this.tubeTypeGroupBox.Location = new System.Drawing.Point(14, 11);
            this.tubeTypeGroupBox.Name = "tubeTypeGroupBox";
            this.tubeTypeGroupBox.Size = new System.Drawing.Size(158, 66);
            this.tubeTypeGroupBox.TabIndex = 3;
            this.tubeTypeGroupBox.TabStop = false;
            this.tubeTypeGroupBox.Text = "Тип трубы";
            // 
            // tubeTypeA
            // 
            this.tubeTypeA.AutoSize = true;
            this.tubeTypeA.Checked = true;
            this.tubeTypeA.Location = new System.Drawing.Point(32, 20);
            this.tubeTypeA.Name = "tubeTypeA";
            this.tubeTypeA.Size = new System.Drawing.Size(90, 17);
            this.tubeTypeA.TabIndex = 1;
            this.tubeTypeA.TabStop = true;
            this.tubeTypeA.Tag = "tubeTypeA";
            this.tubeTypeA.Text = "Стандартная";
            this.tubeTypeA.UseVisualStyleBackColor = true;
            this.tubeTypeA.CheckedChanged += new System.EventHandler(this.Step1_CheckedChanged);
            // 
            // tubeTypeB
            // 
            this.tubeTypeB.AutoSize = true;
            this.tubeTypeB.Location = new System.Drawing.Point(32, 43);
            this.tubeTypeB.Name = "tubeTypeB";
            this.tubeTypeB.Size = new System.Drawing.Size(87, 17);
            this.tubeTypeB.TabIndex = 2;
            this.tubeTypeB.Tag = "tubeTypeB";
            this.tubeTypeB.Text = "Переводник";
            this.tubeTypeB.UseVisualStyleBackColor = true;
            this.tubeTypeB.CheckedChanged += new System.EventHandler(this.Step1_CheckedChanged);
            // 
            // tubePicture
            // 
            this.tubePicture.Image = global::KompasLibrary.Properties.Resources.typeA;
            this.tubePicture.Location = new System.Drawing.Point(215, 31);
            this.tubePicture.Name = "tubePicture";
            this.tubePicture.Size = new System.Drawing.Size(221, 130);
            this.tubePicture.TabIndex = 0;
            this.tubePicture.TabStop = false;
            // 
            // step3Panel
            // 
            this.step3Panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.step3Panel.Controls.Add(this.stairParamsBox);
            this.step3Panel.Controls.Add(this.label17);
            this.step3Panel.Controls.Add(this.label8);
            this.step3Panel.Controls.Add(this.label7);
            this.step3Panel.Controls.Add(this.label6);
            this.step3Panel.Controls.Add(this.label5);
            this.step3Panel.Controls.Add(this.label4);
            this.step3Panel.Controls.Add(this.wallThicknessTxt);
            this.step3Panel.Controls.Add(this.mainSizesPicture);
            this.step3Panel.Controls.Add(this.lengthTxt);
            this.step3Panel.Controls.Add(this.lengthLbl);
            this.step3Panel.Controls.Add(this.chanDiameterLbl);
            this.step3Panel.Controls.Add(this.chanDiameterTxt);
            this.step3Panel.Controls.Add(this.diametrTxt);
            this.step3Panel.Controls.Add(this.diameterLbl);
            this.step3Panel.Location = new System.Drawing.Point(0, 60);
            this.step3Panel.Name = "step3Panel";
            this.step3Panel.Size = new System.Drawing.Size(614, 270);
            this.step3Panel.TabIndex = 8;
            // 
            // stairParamsBox
            // 
            this.stairParamsBox.Controls.Add(this.label21);
            this.stairParamsBox.Controls.Add(this.label20);
            this.stairParamsBox.Controls.Add(this.label19);
            this.stairParamsBox.Controls.Add(this.label18);
            this.stairParamsBox.Controls.Add(this.stairLengthTxt);
            this.stairParamsBox.Controls.Add(this.stairDiameterTxt);
            this.stairParamsBox.Location = new System.Drawing.Point(14, 156);
            this.stairParamsBox.Name = "stairParamsBox";
            this.stairParamsBox.Size = new System.Drawing.Size(242, 73);
            this.stairParamsBox.TabIndex = 15;
            this.stairParamsBox.TabStop = false;
            this.stairParamsBox.Text = "Ступень";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(69, 46);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(89, 13);
            this.label21.TabIndex = 18;
            this.label21.Text = "Ширина ступени";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(64, 22);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(96, 13);
            this.label20.TabIndex = 16;
            this.label20.Text = "Диаметр ступени";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(217, 46);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(23, 13);
            this.label19.TabIndex = 17;
            this.label19.Text = "мм";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(217, 22);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(23, 13);
            this.label18.TabIndex = 16;
            this.label18.Text = "мм";
            // 
            // stairLengthTxt
            // 
            this.stairLengthTxt.BackColor = System.Drawing.SystemColors.Window;
            this.stairLengthTxt.Location = new System.Drawing.Point(164, 43);
            this.stairLengthTxt.MaxLength = 6;
            this.stairLengthTxt.Name = "stairLengthTxt";
            this.stairLengthTxt.Size = new System.Drawing.Size(49, 20);
            this.stairLengthTxt.TabIndex = 7;
            this.stairLengthTxt.Tag = "stairLengthTxt";
            this.stairLengthTxt.Enter += new System.EventHandler(this.txtStep3_Enter);
            this.stairLengthTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mainSetsTxt_KeyPress);
            this.stairLengthTxt.Leave += new System.EventHandler(this.txtStep3_Leave);
            this.stairLengthTxt.Validating += new System.ComponentModel.CancelEventHandler(this.inputTxt_Validating);
            this.stairLengthTxt.Validated += new System.EventHandler(this.inputTxt_Validated);
            // 
            // stairDiameterTxt
            // 
            this.stairDiameterTxt.BackColor = System.Drawing.SystemColors.Window;
            this.stairDiameterTxt.Location = new System.Drawing.Point(164, 19);
            this.stairDiameterTxt.MaxLength = 6;
            this.stairDiameterTxt.Name = "stairDiameterTxt";
            this.stairDiameterTxt.Size = new System.Drawing.Size(49, 20);
            this.stairDiameterTxt.TabIndex = 6;
            this.stairDiameterTxt.Tag = "stairDiameterTxt";
            this.stairDiameterTxt.Enter += new System.EventHandler(this.txtStep3_Enter);
            this.stairDiameterTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mainSetsTxt_KeyPress);
            this.stairDiameterTxt.Leave += new System.EventHandler(this.txtStep3_Leave);
            this.stairDiameterTxt.Validating += new System.ComponentModel.CancelEventHandler(this.inputTxt_Validating);
            this.stairDiameterTxt.Validated += new System.EventHandler(this.inputTxt_Validated);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label17.Location = new System.Drawing.Point(43, 232);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(359, 17);
            this.label17.TabIndex = 11;
            this.label17.Text = "На рисунке представлены настраиваемые элементы";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(233, 127);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(23, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "мм";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(233, 101);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(23, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "мм";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(233, 75);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(23, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "мм";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(233, 49);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(23, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "мм";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 127);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(163, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Минимальная толщина стенки";
            // 
            // wallThicknessTxt
            // 
            this.wallThicknessTxt.Location = new System.Drawing.Point(178, 124);
            this.wallThicknessTxt.MaxLength = 6;
            this.wallThicknessTxt.Name = "wallThicknessTxt";
            this.wallThicknessTxt.Size = new System.Drawing.Size(49, 20);
            this.wallThicknessTxt.TabIndex = 9;
            this.wallThicknessTxt.Tag = "wallThicknessTxt";
            this.wallThicknessTxt.Enter += new System.EventHandler(this.txtStep3_Enter);
            this.wallThicknessTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mainSetsTxt_KeyPress);
            this.wallThicknessTxt.Leave += new System.EventHandler(this.txtStep3_Leave);
            this.wallThicknessTxt.Validating += new System.ComponentModel.CancelEventHandler(this.inputTxt_Validating);
            this.wallThicknessTxt.Validated += new System.EventHandler(this.inputTxt_Validated);
            // 
            // mainSizesPicture
            // 
            this.mainSizesPicture.Image = global::KompasLibrary.Properties.Resources.stairSizes;
            this.mainSizesPicture.Location = new System.Drawing.Point(310, 25);
            this.mainSizesPicture.Name = "mainSizesPicture";
            this.mainSizesPicture.Size = new System.Drawing.Size(285, 171);
            this.mainSizesPicture.TabIndex = 8;
            this.mainSizesPicture.TabStop = false;
            // 
            // step2Panel
            // 
            this.step2Panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.step2Panel.Controls.Add(this.cautionLabelStp2);
            this.step2Panel.Controls.Add(this.label3);
            this.step2Panel.Controls.Add(this.label2);
            this.step2Panel.Controls.Add(this.label1);
            this.step2Panel.Controls.Add(this.rightThreadComboBox);
            this.step2Panel.Controls.Add(this.leftThreadComboBox);
            this.step2Panel.Controls.Add(this.leftThreadPicture);
            this.step2Panel.Controls.Add(this.rightThreadPicture);
            this.step2Panel.Location = new System.Drawing.Point(0, 60);
            this.step2Panel.Name = "step2Panel";
            this.step2Panel.Size = new System.Drawing.Size(614, 270);
            this.step2Panel.TabIndex = 14;
            // 
            // cautionLabelStp2
            // 
            this.cautionLabelStp2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cautionLabelStp2.Location = new System.Drawing.Point(20, 236);
            this.cautionLabelStp2.Name = "cautionLabelStp2";
            this.cautionLabelStp2.Size = new System.Drawing.Size(575, 23);
            this.cautionLabelStp2.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(9, 2);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(561, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "На рисунках представлены параметры конструктивного элемента, зависимые от типа ре" +
    "зьбы";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(333, 206);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(144, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Тип резьбы правого конца";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 206);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(138, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Тип резьбы левого конца";
            // 
            // rightThreadComboBox
            // 
            this.rightThreadComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.rightThreadComboBox.FormattingEnabled = true;
            this.rightThreadComboBox.Location = new System.Drawing.Point(487, 203);
            this.rightThreadComboBox.Name = "rightThreadComboBox";
            this.rightThreadComboBox.Size = new System.Drawing.Size(85, 21);
            this.rightThreadComboBox.TabIndex = 3;
            this.rightThreadComboBox.Tag = "rightThreadComboBox";
            this.rightThreadComboBox.SelectedIndexChanged += new System.EventHandler(this.ComboBoxStep2_SelectedIndexChanged);
            // 
            // leftThreadComboBox
            // 
            this.leftThreadComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.leftThreadComboBox.FormattingEnabled = true;
            this.leftThreadComboBox.Location = new System.Drawing.Point(185, 203);
            this.leftThreadComboBox.Name = "leftThreadComboBox";
            this.leftThreadComboBox.Size = new System.Drawing.Size(85, 21);
            this.leftThreadComboBox.TabIndex = 2;
            this.leftThreadComboBox.Tag = "leftThreadComboBox";
            this.leftThreadComboBox.SelectedIndexChanged += new System.EventHandler(this.ComboBoxStep2_SelectedIndexChanged);
            // 
            // leftThreadPicture
            // 
            this.leftThreadPicture.Image = global::KompasLibrary.Properties.Resources.threadDefNLeft;
            this.leftThreadPicture.Location = new System.Drawing.Point(68, 27);
            this.leftThreadPicture.Name = "leftThreadPicture";
            this.leftThreadPicture.Size = new System.Drawing.Size(155, 143);
            this.leftThreadPicture.TabIndex = 1;
            this.leftThreadPicture.TabStop = false;
            // 
            // rightThreadPicture
            // 
            this.rightThreadPicture.Image = global::KompasLibrary.Properties.Resources.threadDefMRight;
            this.rightThreadPicture.Location = new System.Drawing.Point(391, 25);
            this.rightThreadPicture.Name = "rightThreadPicture";
            this.rightThreadPicture.Size = new System.Drawing.Size(155, 143);
            this.rightThreadPicture.TabIndex = 0;
            this.rightThreadPicture.TabStop = false;
            // 
            // step4Panel
            // 
            this.step4Panel.Controls.Add(this.label15);
            this.step4Panel.Controls.Add(this.keySetsPicture);
            this.step4Panel.Controls.Add(this.label14);
            this.step4Panel.Controls.Add(this.label13);
            this.step4Panel.Controls.Add(this.label12);
            this.step4Panel.Controls.Add(this.label11);
            this.step4Panel.Controls.Add(this.label10);
            this.step4Panel.Controls.Add(this.label9);
            this.step4Panel.Controls.Add(this.keyOffsetTxt);
            this.step4Panel.Controls.Add(this.keyLengthTxt);
            this.step4Panel.Controls.Add(this.keySizeTxt);
            this.step4Panel.Location = new System.Drawing.Point(0, 60);
            this.step4Panel.Name = "step4Panel";
            this.step4Panel.Size = new System.Drawing.Size(614, 270);
            this.step4Panel.TabIndex = 15;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label15.Location = new System.Drawing.Point(12, 236);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(359, 17);
            this.label15.TabIndex = 10;
            this.label15.Text = "На рисунке представлены настраиваемые элементы";
            // 
            // keySetsPicture
            // 
            this.keySetsPicture.Image = global::KompasLibrary.Properties.Resources.keyTypeRoundSizes;
            this.keySetsPicture.Location = new System.Drawing.Point(287, 26);
            this.keySetsPicture.Name = "keySetsPicture";
            this.keySetsPicture.Size = new System.Drawing.Size(309, 180);
            this.keySetsPicture.TabIndex = 9;
            this.keySetsPicture.TabStop = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(233, 102);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(23, 13);
            this.label14.TabIndex = 8;
            this.label14.Text = "мм";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(233, 128);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(23, 13);
            this.label13.TabIndex = 7;
            this.label13.Text = "мм";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(66, 102);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(107, 13);
            this.label12.TabIndex = 6;
            this.label12.Text = "Смещение от торца";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(38, 128);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(135, 13);
            this.label11.TabIndex = 5;
            this.label11.Text = "Ширина ключевого упора";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(233, 76);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(23, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "мм";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(38, 76);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(135, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "Размер ключевого упора";
            // 
            // keyOffsetTxt
            // 
            this.keyOffsetTxt.Location = new System.Drawing.Point(179, 99);
            this.keyOffsetTxt.MaxLength = 6;
            this.keyOffsetTxt.Name = "keyOffsetTxt";
            this.keyOffsetTxt.Size = new System.Drawing.Size(49, 20);
            this.keyOffsetTxt.TabIndex = 2;
            this.keyOffsetTxt.Tag = "keyOffsetTxt";
            this.keyOffsetTxt.Enter += new System.EventHandler(this.txtStep4_Enter);
            this.keyOffsetTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mainSetsTxt_KeyPress);
            this.keyOffsetTxt.Leave += new System.EventHandler(this.txtStep4_Leave);
            this.keyOffsetTxt.Validating += new System.ComponentModel.CancelEventHandler(this.inputTxt_Validating);
            this.keyOffsetTxt.Validated += new System.EventHandler(this.inputTxt_Validated);
            // 
            // keyLengthTxt
            // 
            this.keyLengthTxt.Location = new System.Drawing.Point(179, 125);
            this.keyLengthTxt.MaxLength = 6;
            this.keyLengthTxt.Name = "keyLengthTxt";
            this.keyLengthTxt.Size = new System.Drawing.Size(49, 20);
            this.keyLengthTxt.TabIndex = 1;
            this.keyLengthTxt.Tag = "keyLengthTxt";
            this.keyLengthTxt.Enter += new System.EventHandler(this.txtStep4_Enter);
            this.keyLengthTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mainSetsTxt_KeyPress);
            this.keyLengthTxt.Leave += new System.EventHandler(this.txtStep4_Leave);
            this.keyLengthTxt.Validating += new System.ComponentModel.CancelEventHandler(this.inputTxt_Validating);
            this.keyLengthTxt.Validated += new System.EventHandler(this.inputTxt_Validated);
            // 
            // keySizeTxt
            // 
            this.keySizeTxt.Location = new System.Drawing.Point(179, 73);
            this.keySizeTxt.MaxLength = 6;
            this.keySizeTxt.Name = "keySizeTxt";
            this.keySizeTxt.Size = new System.Drawing.Size(49, 20);
            this.keySizeTxt.TabIndex = 0;
            this.keySizeTxt.Tag = "keySizeTxt";
            this.keySizeTxt.Enter += new System.EventHandler(this.txtStep4_Enter);
            this.keySizeTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mainSetsTxt_KeyPress);
            this.keySizeTxt.Leave += new System.EventHandler(this.txtStep4_Leave);
            this.keySizeTxt.Validating += new System.ComponentModel.CancelEventHandler(this.inputTxt_Validating);
            this.keySizeTxt.Validated += new System.EventHandler(this.inputTxt_Validated);
            // 
            // step5Panel
            // 
            this.step5Panel.Controls.Add(this.label16);
            this.step5Panel.Controls.Add(this.previewPicture);
            this.step5Panel.Controls.Add(this.settingsPreviewTxtBox);
            this.step5Panel.Location = new System.Drawing.Point(0, 60);
            this.step5Panel.Name = "step5Panel";
            this.step5Panel.Size = new System.Drawing.Size(614, 270);
            this.step5Panel.TabIndex = 16;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label16.Location = new System.Drawing.Point(12, 12);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(137, 17);
            this.label16.TabIndex = 2;
            this.label16.Text = "Параметры модели";
            // 
            // previewPicture
            // 
            this.previewPicture.Image = global::KompasLibrary.Properties.Resources.typeA;
            this.previewPicture.Location = new System.Drawing.Point(381, 54);
            this.previewPicture.Name = "previewPicture";
            this.previewPicture.Size = new System.Drawing.Size(221, 130);
            this.previewPicture.TabIndex = 1;
            this.previewPicture.TabStop = false;
            // 
            // settingsPreviewTxtBox
            // 
            this.settingsPreviewTxtBox.Location = new System.Drawing.Point(15, 34);
            this.settingsPreviewTxtBox.Name = "settingsPreviewTxtBox";
            this.settingsPreviewTxtBox.ReadOnly = true;
            this.settingsPreviewTxtBox.Size = new System.Drawing.Size(352, 226);
            this.settingsPreviewTxtBox.TabIndex = 0;
            this.settingsPreviewTxtBox.Text = "";
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // GUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(614, 393);
            this.Controls.Add(this.bottomPanel);
            this.Controls.Add(this.headPanel);
            this.Controls.Add(this.step5Panel);
            this.Controls.Add(this.step1Panel);
            this.Controls.Add(this.step2Panel);
            this.Controls.Add(this.step4Panel);
            this.Controls.Add(this.step3Panel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(630, 432);
            this.Name = "GUI";
            this.Text = "Построение бурильной трубы";
            this.headPanel.ResumeLayout(false);
            this.headPanel.PerformLayout();
            this.bottomPanel.ResumeLayout(false);
            this.step1Panel.ResumeLayout(false);
            this.step1Panel.PerformLayout();
            this.connTypeGroupBox.ResumeLayout(false);
            this.connTypeGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.keyPicture)).EndInit();
            this.keyTypeGroupBox.ResumeLayout(false);
            this.keyTypeGroupBox.PerformLayout();
            this.tubeTypeGroupBox.ResumeLayout(false);
            this.tubeTypeGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tubePicture)).EndInit();
            this.step3Panel.ResumeLayout(false);
            this.step3Panel.PerformLayout();
            this.stairParamsBox.ResumeLayout(false);
            this.stairParamsBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainSizesPicture)).EndInit();
            this.step2Panel.ResumeLayout(false);
            this.step2Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.leftThreadPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightThreadPicture)).EndInit();
            this.step4Panel.ResumeLayout(false);
            this.step4Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.keySetsPicture)).EndInit();
            this.step5Panel.ResumeLayout(false);
            this.step5Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.previewPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button nextBtn;
        private System.Windows.Forms.Button cancelBtn;
        private System.Windows.Forms.Panel headPanel;
        private System.Windows.Forms.Panel bottomPanel;
        private System.Windows.Forms.Label headerLabel;
        private System.Windows.Forms.Button prevBtn;
        private System.Windows.Forms.Label diameterLbl;
        private System.Windows.Forms.TextBox diametrTxt;
        private System.Windows.Forms.TextBox chanDiameterTxt;
        private System.Windows.Forms.Label chanDiameterLbl;
        private System.Windows.Forms.Label lengthLbl;
        private System.Windows.Forms.TextBox lengthTxt;
        private System.Windows.Forms.Panel step1Panel;
        private System.Windows.Forms.Label drawCaptionKTypeLabel;
        private System.Windows.Forms.Label drawCaptionTTypeLabel;
        private System.Windows.Forms.PictureBox keyPicture;
        private System.Windows.Forms.GroupBox keyTypeGroupBox;
        private System.Windows.Forms.RadioButton keyTypeHex;
        private System.Windows.Forms.RadioButton keyTypeRound;
        private System.Windows.Forms.GroupBox tubeTypeGroupBox;
        private System.Windows.Forms.RadioButton tubeTypeA;
        private System.Windows.Forms.RadioButton tubeTypeB;
        private System.Windows.Forms.PictureBox tubePicture;
        private System.Windows.Forms.Panel step3Panel;
        private System.Windows.Forms.GroupBox connTypeGroupBox;
        private System.Windows.Forms.RadioButton connTypeN;
        private System.Windows.Forms.RadioButton connTypeM;
        private System.Windows.Forms.Panel step2Panel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox rightThreadComboBox;
        private System.Windows.Forms.ComboBox leftThreadComboBox;
        private System.Windows.Forms.PictureBox leftThreadPicture;
        private System.Windows.Forms.PictureBox rightThreadPicture;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox wallThicknessTxt;
        private System.Windows.Forms.PictureBox mainSizesPicture;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel step4Panel;
        private System.Windows.Forms.TextBox keyOffsetTxt;
        private System.Windows.Forms.TextBox keyLengthTxt;
        private System.Windows.Forms.TextBox keySizeTxt;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox keySetsPicture;
        private System.Windows.Forms.Panel step5Panel;
        private System.Windows.Forms.PictureBox previewPicture;
        private System.Windows.Forms.RichTextBox settingsPreviewTxtBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label cautionLabelStp2;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.GroupBox stairParamsBox;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox stairLengthTxt;
        private System.Windows.Forms.TextBox stairDiameterTxt;
    }
}