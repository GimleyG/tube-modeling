﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using KompasLibrary.Model;
using KompasLibrary.Model.Types;
using KompasLibrary.Model.Interfaces;
using KompasLibrary.Controller;

namespace KompasLibrary
{
    /// <summary>
    /// Интерфейс предсталвения
    /// </summary>
    public interface IView
    {
        /// <summary>
        /// Доступа к состоянию выделения чекбокса для типа трубы
        /// </summary>
        bool IsTubeTypeAChecked { get; }

        /// <summary>
        /// Доступа к состоянию  выделения чекбокса для типа трубы
        /// </summary>
        bool IsTubeTypeBChecked { get; }

        /// <summary>
        /// Доступа к состоянию  выделения чекбокса для типа ключевого упора
        /// </summary>
        bool IsKeyTypeRoundChecked { get; }

        /// <summary>
        /// Доступа к состоянию  выделения чекбокса для типа ключевого упора
        /// </summary>
        bool IsKeyTypeHexChecked { get; }

        /// <summary>
        /// Доступа к состоянию выделения чекбокса для типа переводника
        /// </summary>
        bool IsConnTypeMChecked { get; }

        /// <summary>
        /// Доступа к состоянию  выделения чекбокса для типа переводника
        /// </summary>
        bool IsConnTypeNChecked { get; }

        /// <summary>
        /// Свойство доступа к выбранному значению в выпадающем списке левой резьбы
        /// </summary>
        string LeftThreadComboBoxCurrent { get; }

        /// <summary>
        /// Свойство доступа к выбранному значению в выпадающем списке правой резьбы
        /// </summary>
        string RightThreadComboBoxCurrent { get; }

        /// <summary>
        /// Свойство доступа к значению, введенному в поле для задания диаметра трубы
        /// </summary>
        double DiameterTxtValue { get; set; }

        /// <summary>
        /// Свойство доступа к значению, введенному в поле для задания длины трубы
        /// </summary>
        double LengthTxtValue { get; set; }

        /// <summary>
        /// Свойство доступа к значению, введенному в поле для задания диаметра канала
        /// </summary>
        double ChanDiameterTxtValue { get; set; }

        /// <summary>
        /// Свойство доступа к значению, введенному в поле для задания толщины стенки
        /// </summary>
        double WallThicknessTxtValue { get; set; }

        /// <summary>
        /// Свойство доступа к значению, введенному в поле для задания длины ступени
        /// </summary>
        double StairLengthTxtBoxValue { get; set; }

        /// <summary>
        /// Свойство доступа к значению, введенному в поле для задания диаметра ступени
        /// </summary>
        double StairDiameterTxtBoxValue { get; set; }
        
        /// <summary>
        /// Свойство доступа к значению, введенному в поле для задания размера ключевого упора
        /// </summary>
        double KeySizeTxtValue { get; set; }

        /// <summary>
        /// Свойство доступа к значению, введенному в поле для задания длины ключевого упора
        /// </summary>
        double KeyLengthTxtValue { get; set; }

        /// <summary>
        /// Свойство доступа к значению, введенному в поле для задания смещения ключевого упора относительно торца
        /// </summary>
        double KeyOffsetTxtValue { get; set; }
    }

    /// <summary>
    /// Класс представления
    /// </summary>
    public partial class GUI : Form, ITubeModelObserver, IView
    {
    #region Поля

        /// <summary>
        /// Ссылка на объект контроллера
        /// </summary>
        private ControllerGUI _controller;

        /// <summary>
        /// Ссылка на объект модели
        /// </summary>
        private ITubeModel _model;

        /// <summary>
        /// Текст в поле ввода параметров, используется при валидации
        /// </summary>
        private string _textInTxtBox;

        /// <summary>
        /// Флаг имеющейся ошибки ввода
        /// </summary>
        private bool _hasInputError;

    #endregion
        /// <summary>
        /// Конструктор представления
        /// </summary>
        /// <param name="controller">Ссылка на объект контроллера</param>
        /// <param name="model">Ссылка на объект модели</param>
        public GUI(ControllerGUI controller, ITubeModel model)
        {
            InitializeComponent();
            _model = model;
            _model.RegisterObserver(this);
            _controller = controller;
            _hasInputError = false;
        }        

    #region Пометка ошибок 

        /// <summary>
        /// Свойство доступа к флагу ошибки ввода
        /// </summary>
        public bool HasInputError
        {
            get { return _hasInputError; }
            set { _hasInputError = value; }
        }

        /// <summary>
        /// Пометка поля ввода параметра модели маркером ошибки
        /// </summary>
        /// <param name="nameOfElem">Название параметра модели</param>
        public void MarkBoxWithErrorParameter(NameOfTubeParameter nameOfElem)
        {
            string errMsg = _model.ErrorReport;
            switch (nameOfElem)
            {
                case NameOfTubeParameter.Diameter:
                    errorProvider.SetError(diametrTxt, errMsg);
                    break;
                case NameOfTubeParameter.Length:
                    errorProvider.SetError(lengthTxt, errMsg);
                    break;
                case NameOfTubeParameter.ChannelDiameter:
                    errorProvider.SetError(chanDiameterTxt, errMsg);
                    break;
                case NameOfTubeParameter.WallThickness:
                    errorProvider.SetError(wallThicknessTxt, errMsg);
                    break;
                case NameOfTubeParameter.KeyLength:
                    errorProvider.SetError(keyLengthTxt, errMsg);
                    break;
                case NameOfTubeParameter.KeySize:
                    errorProvider.SetError(keySizeTxt, errMsg);
                    break;
                case NameOfTubeParameter.KeyOffset:
                    errorProvider.SetError(keyOffsetTxt, errMsg);
                    break;
                case NameOfTubeParameter.StairLength:
                    errorProvider.SetError(stairLengthTxt, errMsg);
                    break;
                case NameOfTubeParameter.StairDiameter:
                    errorProvider.SetError(stairDiameterTxt, errMsg);
                    break;
            }
        }

    #endregion

    #region Свойства доступа к элементам шагов

        /// <summary>
        /// Свойство доступа к строке заголовка шага
        /// </summary>
        public string StepTitle
        {
            get { return headerLabel.Text;}
            set { headerLabel.Text = value; }
        }

        /// <summary>
        /// Свойство доступа к надписи на кнопке перехода к следующему шагу
        /// </summary>
        public string NextBtnCapture
        {
            get { return nextBtn.Text; }
            set { nextBtn.Text = value; }
        }

        /// <summary>
        /// Свойство для установки доступности кнопки перехода к предыдущему шагу
        /// </summary>
        public bool IsPrevButtonEnabled
        {
            get { return prevBtn.Enabled; }
            set { prevBtn.Enabled = value; }
        }

        /// <summary>
        /// Свойство для получения списка панелей шагов
        /// </summary>
        public List<Panel> StepPanels
        {
            get
            {
                List<Panel> steps = new List<Panel>();
                steps.Add(this.step1Panel);
                steps.Add(this.step2Panel);
                steps.Add(this.step3Panel);
                steps.Add(this.step4Panel);
                steps.Add(this.step5Panel);
                return steps;
            }
        }

    #endregion

    #region Свойства для доступа к значениям элементов настройки модели

        #region с первого экрана

        /// <summary>
        /// Свойство для изменения видимости группы флагов выбора типа коннектора
        /// </summary>
        public bool IsConnTypeGroupBoxVisible
        {
            set
            {
                connTypeGroupBox.Visible = value;
                if (value)
                {
                    connTypeGroupBox.Location = new Point(14, 98);
                    keyTypeGroupBox.Location = new Point(14, 189);
                }
                else
                {
                    connTypeGroupBox.Location = new Point(14, 189);
                    keyTypeGroupBox.Location = new Point(14, 98);
                }
            }
        }

        /// <summary>
        /// Свойство проверки установки флага стандартного типа трубы
        /// </summary>
        public bool IsTubeTypeAChecked
        {
            get { return tubeTypeA.Checked; }
        }

        /// <summary>
        /// Свойство для проверки установки флага типа трубы как коннектора
        /// </summary>
        public bool IsTubeTypeBChecked
        {
            get { return tubeTypeB.Checked; }
        }

        /// <summary>
        /// Свойство для проверки что выбран тип ключа круглый
        /// </summary>
        public bool IsKeyTypeRoundChecked
        {
            get { return keyTypeRound.Checked; }
        }

        /// <summary>
        /// Свойство для проверки что выбран тип ключа шестигранник
        /// </summary>
        public bool IsKeyTypeHexChecked
        {
            get { return keyTypeHex.Checked; }
        }

        /// <summary>
        /// Свойство для проверки что выбран муфтовый переводник
        /// </summary>
        public bool IsConnTypeMChecked
        {
            get { return connTypeM.Checked; }
        }

        /// <summary>
        /// Свойство для проверки что выбран ниппельный переводник
        /// </summary>
        public bool IsConnTypeNChecked
        {
            get { return connTypeN.Checked; }
        }

        /// <summary>
        /// Свойство доступа к подписи изображения типа трубы
        /// </summary>
        public string TubeTypeLabel
        {
            get{ return drawCaptionTTypeLabel.Text; }
            set{ drawCaptionTTypeLabel.Text = value; }             
        }

        /// <summary>
        /// Свойство доступа к подписи изображения типа ключа
        /// </summary>
        public string KeyTypeLabel
        {
            get{ return drawCaptionKTypeLabel.Text; }
            set{ drawCaptionKTypeLabel.Text = value; }            
        }

        /// <summary>
        /// Свойство для доступа к изображению типа трубы
        /// </summary>
        public Bitmap TubePicture
        {
            get{ return (Bitmap)tubePicture.Image;}
            set{ tubePicture.Image = value;}
        }

        /// <summary>
        /// Свойство для доступа к изображению типа ключа
        /// </summary>
        public Bitmap KeyPicture
        {
            get{ return (Bitmap)keyPicture.Image;}
            set{ keyPicture.Image = value;}
        }
        #endregion

        #region со второго экрана
        public string LeftThreadComboBoxCurrent
        {
            get { return (string)leftThreadComboBox.SelectedItem; }
        }

        public string RightThreadComboBoxCurrent
        {
            get { return (string)rightThreadComboBox.SelectedItem; }
        }
        #endregion

        #region с третьего экрана

        public double DiameterTxtValue
        {
            get{ return Convert.ToDouble(diametrTxt.Text);}
            set { diametrTxt.Text = Convert.ToString(value); }
            
        }

        public double LengthTxtValue
        {
            get { return Convert.ToDouble(lengthTxt.Text); }
            set { lengthTxt.Text = Convert.ToString(value); }            
        }

        public double ChanDiameterTxtValue
        {
            get { return Convert.ToDouble(chanDiameterTxt.Text); }
            set { chanDiameterTxt.Text = Convert.ToString(value); }              
        }

        public double WallThicknessTxtValue
        {
            get { return Convert.ToDouble(wallThicknessTxt.Text); }
            set { wallThicknessTxt.Text = Convert.ToString(value); }               
        }

        public double StairLengthTxtBoxValue
        {
            get { return Convert.ToDouble(stairLengthTxt.Text); }
            set { stairLengthTxt.Text = Convert.ToString(value); }
        }

        public double StairDiameterTxtBoxValue
        {
            get { return Convert.ToDouble(stairDiameterTxt.Text); }
            set { stairDiameterTxt.Text = Convert.ToString(value); }
        }

        public Bitmap MainSizesPicture
        {
            get { return (Bitmap)mainSizesPicture.Image; }
            set { mainSizesPicture.Image = value; }
        }

        #endregion

        #region с четвертого экрана
        public double KeySizeTxtValue
        {
            get { return Convert.ToDouble(keySizeTxt.Text); }
            set { keySizeTxt.Text = Convert.ToString(value); }              
        }

        public double KeyLengthTxtValue
        {
            get { return Convert.ToDouble(keyLengthTxt.Text); }
            set { keyLengthTxt.Text = Convert.ToString(value); }              
        }

        public double KeyOffsetTxtValue
        {
            get { return Convert.ToDouble(keyOffsetTxt.Text);}
            set { keyOffsetTxt.Text = Convert.ToString(value); } 
        }

        public Bitmap KeySetsPicture
        {
            get { return (Bitmap)keySetsPicture.Image; }
            set { keySetsPicture.Image = value; }
        }
        #endregion

    #endregion               

    #region События навигации
    
        /// <summary>
        /// Обработка нажатия кнопки перехода к следующему экрану
        /// </summary>
        /// <param name="sender">Объект инициатор</param>
        /// <param name="e">Параметры события</param>
        private void nextBtn_Click(object sender, EventArgs e)
        {
            _controller.Next();            
        }

        /// <summary>
        /// Обработка нажатия кнопки отмены
        /// </summary>
        /// <param name="sender">Объект инициатор</param>
        /// <param name="e">Параметры события</param>
        private void cancelBtn_Click(object sender, EventArgs e)
        {
            _controller.Stop();
        }

        /// <summary>
        /// Обработка нажатия кнопки перехода к предыдущему экрану
        /// </summary>
        /// <param name="sender">Объект инициатор</param>
        /// <param name="e">Параметры события</param>
        private void prevBtn_Click(object sender, EventArgs e)
        {
            _controller.Prev();
        }

    #endregion

    #region Step1 events

        /// <summary>
        /// Обработка события смены выбора флагов с первого шага
        /// </summary>
        /// <param name="sender">Объект инициатор</param>
        /// <param name="e">Параметры события</param>
        private void Step1_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton btn = sender as RadioButton;
            if (btn != null && btn.Checked)
            {
                switch ((string)btn.Tag)
                {
                    case "tubeTypeA":
                        _controller.ChangedParameter(NameOfTubeParameter.TypeOfTube);
                        break;
                    case "tubeTypeB":
                        _controller.ChangedParameter(NameOfTubeParameter.TypeOfTube);
                        break;
                    case "keyTypeRound":
                        _controller.ChangedParameter(NameOfTubeParameter.TypeOfKey);
                        break;
                    case "keyTypeHex":
                        _controller.ChangedParameter(NameOfTubeParameter.TypeOfKey);
                        break;
                    case "connTypeM":
                        _controller.ChangedParameter(NameOfTubeParameter.TypeOfTube);
                        break;
                    case "connTypeN":
                        _controller.ChangedParameter(NameOfTubeParameter.TypeOfTube);
                        break;
                }
            }
        }

    #endregion
     
    #region Step2 events

        /// <summary>
        /// Обработка события выбора нового элемента списка на втором шаге
        /// </summary>
        /// <param name="sender">Объект инициатор</param>
        /// <param name="e">Параметры события</param>
        private void ComboBoxStep2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox obj = sender as ComboBox;
            switch ((string)obj.Tag)
            {
                case "leftThreadComboBox":
                    _controller.ChangedParameter(NameOfTubeParameter.LeftSideThread);
                    break;
                case "rightThreadComboBox":
                    _controller.ChangedParameter(NameOfTubeParameter.RightSideThread);
                    break;
            }
        }

    #endregion

    #region Step3 events
        
        /// <summary>
        /// Обработка события получения фокуса полем ввода
        /// </summary>
        /// <param name="sender">Объект инициатор</param>
        /// <param name="e">Параметры события</param>
        private void txtStep3_Enter(object sender, EventArgs e)
        {
            TextBox field = sender as TextBox;
            field.BackColor = Color.LightCyan;
            _textInTxtBox = field.Text;

            Bitmap curPic = null;
            switch (_model.Parameters.TypeOfTube)
            {
                case TubeType.Standart:
                    curPic = Properties.Resources.mainSizesTypeA;
                    break;
                case TubeType.ConnectorMuf:
                    curPic = Properties.Resources.mainSizesTypeB;
                    break;
                case TubeType.ConnectorNip:
                    curPic = Properties.Resources.mainSizesTypeB1;
                    break;                
            }

            switch ((string)field.Tag)
            {
                case "lengthTxt":
                    mainSizesPicture.Image = curPic;
                    _controller.SetHighlightingOfParameter(NameOfTubeParameter.Length);                    
                    break;
                case "diametrTxt":
                    mainSizesPicture.Image = curPic;
                    _controller.SetHighlightingOfParameter(NameOfTubeParameter.Diameter);       
                    break;
                case "chanDiameterTxt":
                    mainSizesPicture.Image = curPic;
                    _controller.SetHighlightingOfParameter(NameOfTubeParameter.ChannelDiameter);                    
                    break;
                case "wallThicknessTxt":
                    mainSizesPicture.Image = curPic;
                    _controller.SetHighlightingOfParameter(NameOfTubeParameter.WallThickness);                    
                    break;
                case "stairDiameterTxt":
                    mainSizesPicture.Image = Properties.Resources.stairSizes;
                    _controller.SetHighlightingOfParameter(NameOfTubeParameter.StairDiameter);
                    break;
                case "stairLengthTxt":
                    mainSizesPicture.Image = Properties.Resources.stairSizes;
                    _controller.SetHighlightingOfParameter(NameOfTubeParameter.StairLength);
                    break;
            }
        }

        /// <summary>
        /// Обработка события потери фокуса полем ввода
        /// </summary>
        /// <param name="sender">Объект инициатор</param>
        /// <param name="e">Параметры события</param>
        private void txtStep3_Leave(object sender, EventArgs e)
        {     
            TextBox field = sender as TextBox;
            field.BackColor = Color.White;
            if (!_textInTxtBox.Equals(field.Text) && field.Text != "")
            {
                switch ((string)field.Tag)
                {
                    case "diametrTxt":
                        _controller.ChangedParameter(NameOfTubeParameter.Diameter);
                        break;
                    case "lengthTxt":
                        _controller.ChangedParameter(NameOfTubeParameter.Length);
                        break;
                    case "chanDiameterTxt":
                        _controller.ChangedParameter(NameOfTubeParameter.ChannelDiameter);
                        break;
                    case "wallThicknessTxt":
                        _controller.ChangedParameter(NameOfTubeParameter.WallThickness);
                        break;
                    case "stairDiameterTxt":
                        _controller.ChangedParameter(NameOfTubeParameter.StairDiameter);
                        break;
                    case "stairLengthTxt":
                        _controller.ChangedParameter(NameOfTubeParameter.StairLength);
                        break;
                }

            }
            if (!_hasInputError)
            {
                switch ((string)field.Tag)
                {
                    case "lengthTxt":
                        _controller.RefuseHighlightingOfParameter(NameOfTubeParameter.Length);
                        break;
                    case "diametrTxt":
                        _controller.RefuseHighlightingOfParameter(NameOfTubeParameter.Diameter);
                        break;
                    case "chanDiameterTxt":
                        _controller.RefuseHighlightingOfParameter(NameOfTubeParameter.ChannelDiameter);
                        break;
                    case "wallThicknessTxt":
                        _controller.RefuseHighlightingOfParameter(NameOfTubeParameter.WallThickness);
                        break;
                    case "stairDiameterTxt":
                        _controller.RefuseHighlightingOfParameter(NameOfTubeParameter.StairDiameter);
                        break;
                    case "stairLengthTxt":
                        _controller.RefuseHighlightingOfParameter(NameOfTubeParameter.StairLength);
                        break;
                }
            }
        }              

    #endregion    
    
    #region Step4 events
        /// <summary>
        /// Обработка события получения фокуса полем ввода на четвертом шаге
        /// </summary>
        /// <param name="sender">Объект инициатор</param>
        /// <param name="e">Параметры события</param>
        private void txtStep4_Enter(object sender, EventArgs e)
        {
            TextBox field = sender as TextBox;
            field.BackColor = Color.LightCyan;
            _textInTxtBox = field.Text;

            switch ((string)field.Tag)
            {
                case "keySizeTxt":
                    _controller.SetHighlightingOfParameter(NameOfTubeParameter.KeySize);
                    break;
                case "keyOffsetTxt":
                    _controller.SetHighlightingOfParameter(NameOfTubeParameter.KeyOffset);
                    break;
                case "keyLengthTxt":
                    _controller.SetHighlightingOfParameter(NameOfTubeParameter.KeyLength);
                    break;
            }
        }

        /// <summary>
        /// Обработка события потери фокуса полем ввода на четвертом шаге
        /// </summary>
        /// <param name="sender">Объект инициатор</param>
        /// <param name="e">Параметры события</param>
        private void txtStep4_Leave(object sender, EventArgs e)
        {
            TextBox field = sender as TextBox;
            field.BackColor = Color.White;
            if (!_textInTxtBox.Equals(field.Text) && field.Text != "")
            {
                switch ((string)field.Tag)
                {
                    case "keyLengthTxt":
                        _controller.ChangedParameter(NameOfTubeParameter.KeyLength);
                        break;
                    case "keyOffsetTxt":
                        _controller.ChangedParameter(NameOfTubeParameter.KeyOffset);
                        break;
                    case "keySizeTxt":
                        _controller.ChangedParameter(NameOfTubeParameter.KeySize);
                        break;
                }
            }
            if (!_hasInputError)
            {
                switch ((string)field.Tag)
                {
                    case "keySizeTxt":
                        _controller.RefuseHighlightingOfParameter(NameOfTubeParameter.KeySize);
                        break;
                    case "keyOffsetTxt":
                        _controller.RefuseHighlightingOfParameter(NameOfTubeParameter.KeyOffset);
                        break;
                    case "keyLengthTxt":
                        _controller.RefuseHighlightingOfParameter(NameOfTubeParameter.KeyLength);
                        break;
                }
            }
        }

    #endregion 
    
    #region Общие события текстовых полей

        /// <summary>
        /// Обработка события ввода символов в поля ввода на 3-ем и 4-ом шаге
        /// </summary>
        /// <param name="sender">Объект инициатор</param>
        /// <param name="e">Параметры события</param>
        private void mainSetsTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((Keys)e.KeyChar == Keys.Back)
                return;

            if ((uint)e.KeyChar == 44)
            {
                foreach (char ch in ((TextBox)sender).Text)
                {
                    if ((uint)ch == 44)
                        e.Handled = true;
                }
                return;
            }

            if ((uint)e.KeyChar < 48 || (uint)e.KeyChar > 57)
            {
                e.Handled = true;
            }
            else
                return;
        }

        /// <summary>
        /// Обработка события валидации поля ввода
        /// </summary>
        /// <param name="sender">Объект инициатор</param>
        /// <param name="e">Параметры события</param>
        private void inputTxt_Validating(object sender, CancelEventArgs e)
        {
            TextBox txt = sender as TextBox;
            if (txt.Text == "")
            {
                e.Cancel = true;
                errorProvider.SetError(txt, "Пустое поле!");
            }
            else if (_hasInputError)
            {
                e.Cancel = true;
                string errMsg = _model.ErrorReport;
                errorProvider.SetError(txt, errMsg);
            }
        }

        /// <summary>
        /// Обработка события возникающего после валидации поля
        /// </summary>
        /// <param name="sender">Объект инициатор</param>
        /// <param name="e">Параметры события</param>
        private void inputTxt_Validated(object sender, EventArgs e)
        {
            TextBox txt = sender as TextBox;
            errorProvider.SetError(txt, "");
        }

    #endregion

    #region Реализация интерфейса ITubeModelObserver

        /// <summary>
        /// Действия при изменении параметра модели тип трубы 
        /// </summary>
        public void ChangedTubeType()
        {
            ITubeParameters param = _model.Parameters;            
            switch (param.TypeOfTube)
            {
                case TubeType.Standart:
                    leftThreadPicture.Image = Properties.Resources.threadDefNLeft;
                    rightThreadPicture.Image = Properties.Resources.threadDefMRight;
                    mainSizesPicture.Image = Properties.Resources.mainSizesTypeA;
                    previewPicture.Image = Properties.Resources.typeA;
                    break;
                case TubeType.ConnectorNip:
                    leftThreadPicture.Image = Properties.Resources.threadDefNLeft;
                    rightThreadPicture.Image = Properties.Resources.threadDefNRight;
                    mainSizesPicture.Image = Properties.Resources.mainSizesTypeB1;
                    previewPicture.Image = Properties.Resources.typeB1;
                    break;
                case TubeType.ConnectorMuf:
                    leftThreadPicture.Image = Properties.Resources.threadDefMLeft;
                    rightThreadPicture.Image = Properties.Resources.threadDefMRight;
                    mainSizesPicture.Image = Properties.Resources.mainSizesTypeB;
                    previewPicture.Image = Properties.Resources.typeB;
                    break;
            }
            settingsPreviewTxtBox.Lines = _model.CreateModelParametersReport();
        }

        /// <summary>
        /// Действия при установке модели к установкам по умолчанию
        /// </summary>
        public void SetDefaultSettings()
        {
            ITubeParameters param = _model.Parameters;            
            //настройки связанные с типом трубы
            switch (param.TypeOfTube)
            {
                case TubeType.Standart:
                    tubeTypeA.Checked = true;
                    this.TubeTypeLabel = "Стандартная труба";
                    tubePicture.Image = Properties.Resources.typeA;

                    leftThreadPicture.Image = Properties.Resources.threadDefNLeft;
                    rightThreadPicture.Image = Properties.Resources.threadDefMRight;
                    mainSizesPicture.Image = Properties.Resources.mainSizesTypeA;
                    previewPicture.Image = Properties.Resources.typeA;
                    break;

                case TubeType.ConnectorNip:
                    this.IsConnTypeGroupBoxVisible = true;
                    connTypeN.Checked = true;
                    this.TubeTypeLabel = "Переводник ниппельный";
                    tubePicture.Image = Properties.Resources.typeB1;

                    leftThreadPicture.Image = Properties.Resources.threadDefNLeft;
                    rightThreadPicture.Image = Properties.Resources.threadDefNRight;
                    mainSizesPicture.Image = Properties.Resources.mainSizesTypeB1;
                    previewPicture.Image = Properties.Resources.typeB1;
                    break;

                case TubeType.ConnectorMuf:
                    this.IsConnTypeGroupBoxVisible = true;
                    connTypeM.Checked = true;
                    this.TubeTypeLabel = "Переводник муфтовый";

                    tubePicture.Image = Properties.Resources.typeB;
                    leftThreadPicture.Image = Properties.Resources.threadDefMLeft;
                    rightThreadPicture.Image = Properties.Resources.threadDefMRight;
                    mainSizesPicture.Image = Properties.Resources.mainSizesTypeB;
                    previewPicture.Image = Properties.Resources.typeB;
                    break;
            }
            //настройки связанный с типом ключевого упора
            switch (param.TypeOfKey)
            {
                case KeyType.Round:
                    this.KeyTypeLabel = "Круговой";
                    this.KeyPicture = Properties.Resources.keyTypeRound;
                    break;
                case KeyType.Hexahedron:
                    this.KeyTypeLabel = "Шестигранник";
                    this.KeyPicture = Properties.Resources.keyTypeHex;
                    break;
            }
            //заполнение элементов ComboBox названиями типов резьб
            List<string> threads = param.ThreadTypeNames;
            foreach (string elem in threads)
            {
                leftThreadComboBox.Items.Add(elem);
                rightThreadComboBox.Items.Add(elem);
            }
            string currLeft = param.LeftSideThreadName;
            leftThreadComboBox.SelectedIndex = leftThreadComboBox.FindStringExact(currLeft);
            string currRight = param.RightSideThreadName;
            rightThreadComboBox.SelectedIndex = rightThreadComboBox.FindStringExact(currRight);

            //заполнение основных параметров
            this.DiameterTxtValue      = param.Diameter;
            this.LengthTxtValue        = param.Length;
            this.ChanDiameterTxtValue  = param.ChannelDiameter;
            this.WallThicknessTxtValue = param.WallThickness;

            stairParamsBox.Visible = false;

            //заполнение параметров ключевого упора
            this.KeySizeTxtValue   = param.KeySize;
            this.KeyLengthTxtValue = param.KeyLength;
            this.KeyOffsetTxtValue = param.KeyOffset;

            //заполнение окна Preview
            settingsPreviewTxtBox.Lines = _model.CreateModelParametersReport();
        }        

        /// <summary>
        /// Действия при изменении параметра модели тип ключа
        /// </summary>
        public void ChangedKeyType()
        {
            ITubeParameters param = _model.Parameters;     
            switch (param.TypeOfKey)
            {
                case KeyType.Hexahedron:
                    keySetsPicture.Image = Properties.Resources.keyTypeHexSizes;
                    break;
                case KeyType.Round:
                    keySetsPicture.Image = Properties.Resources.keyTypeRoundSizes;
                    break;   
            }
            settingsPreviewTxtBox.Lines = _model.CreateModelParametersReport();
        }

        /// <summary>
        /// Действия при изменении параметров модели
        /// </summary>
        public void ChangedSizesOfTubeElements()
        {
            settingsPreviewTxtBox.Lines = _model.CreateModelParametersReport();
        }

        /// <summary>
        /// Действия при изменении необходимости построения ступени
        /// </summary>
        public void ChangedStairNeed()
        {
            ITubeParameters param = _model.Parameters;
            if (param.IsNeedStair)
            {
                stairParamsBox.Visible = true;
                this.StairDiameterTxtBoxValue = param.StairDiameter;
                this.StairLengthTxtBoxValue   = param.StairLength;
            }
            else
            {
                stairParamsBox.Visible = false;
            }
            settingsPreviewTxtBox.Lines = _model.CreateModelParametersReport();
        }
    #endregion

    }

}
