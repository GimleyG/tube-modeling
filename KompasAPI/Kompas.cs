﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kompas6API5;
using Kompas6Constants3D;
using System.Drawing;
using Kompas6Constants;

namespace KompasLibrary.KompasAPI
{
    /// <summary>
    /// Класс-обертка для упращения работы с API Компаса
    /// </summary>
    public class KompasWrapper
    {
        /// <summary>
        /// Основной интерфейс API
        /// </summary>
        private KompasObject _kompasObj;

        /// <summary>
        /// Интерфейс трехмерного документа
        /// </summary>
        private ksDocument3D _doc3D;

        /// <summary>
        /// Интерфейс трехмерной детали
        /// </summary>
        private ksPart _part;

        /// <summary>
        /// конструктор создания объекта
        /// </summary>
        /// <param name="kompas">Ссылка на основной интерфейс API Компаса</param>
        public KompasWrapper(KompasObject kompas)
        {
            _kompasObj = kompas;            
        }

        /// <summary>
        /// Инициализация трехмерного документа и создание детали
        /// </summary>
        public void Initialize()
        {
            InitDocument();
            _part = _doc3D.GetPart((short)Part_Type.pNew_Part);
        }
        
        /// <summary>
        /// Действия перед уничтожением объекта
        /// </summary>
        public void Dispose()
        {
            _kompasObj = null;
        }

        /// <summary>
        /// Свойство для доступа к основному интерфейсу API Компаса, только чтение
        /// </summary>
        public KompasObject KompasObj
        {
            get { return _kompasObj; }
        }

        /// <summary>
        /// Свойство для досутпа к интерфейсу трехмерного документа, только чтение
        /// </summary>
        public ksDocument3D Document3D
        {
            get { return _doc3D; }
        }

        /// <summary>
        /// Свойство для доступа к интерфейсу трехмерной детали, только чтение
        /// </summary>
        public ksPart Part
        {
            get { return _part; }
        }

        /// <summary>
        /// Инициализация трехмерного документа
        /// </summary>
        private void InitDocument()
        {
            _doc3D = (ksDocument3D)_kompasObj.Document3D();
            _doc3D.Create(false, true);
            _doc3D.author = "Ivan";
            _doc3D.comment = "Tube 3D";
            _doc3D.drawMode = 3;
            _doc3D.perspective = true;
            _doc3D.UpdateDocumentParam();
        }        

        /// <summary>
        /// Вывод сообщения в окне Компаса
        /// </summary>
        /// <param name="msg">Текст сообщения</param>
        public void ShowMessage(string msg)
        {
            _kompasObj.ksMessage(msg);
        }

        /// <summary>
        /// Свойство для получения плоскости XOY, только чтение
        /// </summary>
        public ksEntity PlaneXOY
        {
            get { return (ksEntity)_part.GetDefaultEntity((short)Obj3dType.o3d_planeXOY); }
        }

        /// <summary>
        /// Свойство для получения плоскости XOZ, только чтение
        /// </summary>
        public ksEntity PlaneXOZ
        {
            get{ return (ksEntity)_part.GetDefaultEntity((short)Obj3dType.o3d_planeXOZ); }
        }

        /// <summary>
        /// Свойство для получения плоскости YOZ, только чтение
        /// </summary>
        public ksEntity PlaneYOZ
        {
            get { return (ksEntity)_part.GetDefaultEntity((short)Obj3dType.o3d_planeYOZ); }
        }

        /// <summary>
        /// Свойство для получения пустого массива сущностей 
        /// </summary>
        public ksEntityCollection BlankEntityCollection
        {
            get {
                ksEntityCollection collection = (ksEntityCollection)_part.EntityCollection((short)Obj3dType.o3d_sketch);
                collection.Clear();
                return collection;
                }
        }

    #region Создание эскизов
        /// <summary>
        /// Создание эскиза окружности
        /// </summary>
        /// <param name="plane">Опорная плоскость</param>
        /// <param name="center">Точка центра</param>
        /// <param name="radius">Величина радиуса</param>
        /// <returns>Интерфейс сущности эскиза</returns>
        public ksEntity CreateCircleSketch(ksEntity plane, PointDouble center, double radius)
        {
            ksEntity sketch = (ksEntity)_part.NewEntity((short)Obj3dType.o3d_sketch);
            ksSketchDefinition sketchDef = (ksSketchDefinition)sketch.GetDefinition();
            
            sketchDef.SetPlane(plane);	
            sketch.Create();			
            
            ksDocument2D sketchEdit = (ksDocument2D)sketchDef.BeginEdit();
            sketchEdit.ksCircle(center.X, center.Y, radius, 1);
            sketchDef.EndEdit();

            return sketch;
        }

        /// <summary>
        /// Создание эскиза кольца
        /// </summary>
        /// <param name="plane">Опорная плоскость</param>
        /// <param name="center">Точка центра</param>
        /// <param name="radiusBig">Большой радиус</param>
        /// <param name="radiusSmall">Маленький радиус</param>
        /// <returns>Интерфейс сущности эскиза</returns>
        public ksEntity CreateRingSketch(ksEntity plane, PointDouble center, double radiusBig, double radiusSmall)
        {
            ksEntity sketch = (ksEntity)_part.NewEntity((short)Obj3dType.o3d_sketch);
            ksSketchDefinition sketchDef = (ksSketchDefinition)sketch.GetDefinition();

            sketchDef.SetPlane(plane);
            sketch.Create();

            ksDocument2D sketchEdit = (ksDocument2D)sketchDef.BeginEdit();
            sketchEdit.ksCircle(center.X, center.Y, radiusBig, 1);
            sketchEdit.ksCircle(center.X, center.Y, radiusSmall, 1);
            sketchDef.EndEdit();

            return sketch;
        }

        /// <summary>
        /// Создание эскиза прямоугольника
        /// </summary>
        /// <param name="plane">Опорная плоскость</param>
        /// <param name="startPoint">Точка левого верхнего угла прямоугольника</param>
        /// <param name="endPoint">Точка правого нижнего угла</param>
        /// <returns>Интерфейс сущности эскиза</returns>
        public ksEntity CreateRectangleSketch(ksEntity plane, PointDouble startPoint, PointDouble endPoint)
        {
            ksEntity sketch = (ksEntity)_part.NewEntity((short)Obj3dType.o3d_sketch);
            ksSketchDefinition sketchDef = (ksSketchDefinition)sketch.GetDefinition();

            sketchDef.SetPlane(plane);
            sketch.Create();

            ksDocument2D sketchEdit = (ksDocument2D)sketchDef.BeginEdit();
            // интерфейс редактора эскиза
            ksRectangleParam recPar = (ksRectangleParam)_kompasObj.GetParamStruct((short)StructType2DEnum.ko_RectangleParam);
            recPar.Init();
            if (recPar != null)
            {
                recPar.x = startPoint.X;
                recPar.y = startPoint.Y; 
                recPar.width = endPoint.X;
                recPar.height = endPoint.Y;
                recPar.style = 1;
            }
            sketchEdit.ksRectangle(recPar, 0); 
            sketchDef.EndEdit();

            return sketch;
        }

        /// <summary>
        /// Эскиз правильного шестиугольника
        /// </summary>
        /// <param name="plane">опорная плоскость</param>
        /// <param name="inCircleCenter">Центр вписанной окружности </param>
        /// <param name="inCircleRadius">Радиус вписанной окружности</param>
        /// <returns>Интерфейс сущности эскиза</returns>
        public ksEntity CreateHexahedronSketch(ksEntity plane, PointDouble inCircleCenter, double inCircleRadius)
        {
            ksEntity sketch = (ksEntity)_part.NewEntity((short)Obj3dType.o3d_sketch);
            ksSketchDefinition sketchDef = (ksSketchDefinition)sketch.GetDefinition();

            sketchDef.SetPlane(plane);
            sketch.Create();

            ksDocument2D sketchEdit = (ksDocument2D)sketchDef.BeginEdit();
            // интерфейс редактора эскиза
            ksRegularPolygonParam hexPar = (ksRegularPolygonParam)_kompasObj.GetParamStruct((short)StructType2DEnum.ko_RegularPolygonParam);
            hexPar.Init();
            if (hexPar != null)
            {
                hexPar.count = 6;
                hexPar.xc = inCircleCenter.X;
                hexPar.yc = inCircleCenter.Y;
                hexPar.ang = 0;
                hexPar.radius = inCircleRadius;
                hexPar.style = 1;
            }
            sketchEdit.ksRegularPolygon(hexPar);
            sketchDef.EndEdit();

            return sketch;
        }
    #endregion

    #region Создание формообразующих операций

        /// <summary>
        /// Создание базовой операции выдавливания
        /// </summary>
        /// <param name="sketch">Эскиз выдавливания</param>
        /// <param name="directionType">Тип направления (прямое, обратное, оба)</param>
        /// <param name="endType">Признак окончания выполнения (на глубину, через все и т.д)</param>
        /// <param name="length">Глубина выдавливания</param>
        /// <returns>Интерфейс сущности выдавливания</returns>
        public ksEntity CreateBaseExtrusion(ksEntity sketch, short directionType, short endType, double length)
        {
            //базовая операция выдавливания
            ksEntity extrusion = (ksEntity)_part.NewEntity((short)Obj3dType.o3d_baseExtrusion);

            ksBaseExtrusionDefinition extrusionDef = (ksBaseExtrusionDefinition)extrusion.GetDefinition();

            extrusionDef.directionType = directionType;
            extrusionDef.SetSideParam(true,	
                                        endType,	
                                        length, 
                                        0, 
                                        false);
            extrusionDef.SetSideParam(false,	
                                        endType,	
                                        length,
                                        0,
                                        false);   
            extrusionDef.SetThinParam(false, 0, 0, 0);
            extrusionDef.SetSketch(sketch);

            return extrusion;              
        }

        /// <summary>
        /// Создание операции выреза выдавливанием
        /// </summary>
        /// <param name="sketch">Эскиз выдавливания</param>
        /// <param name="directionType">Тип направления (прямое, обратное, оба)</param>
        /// <param name="endType">Признак окончания выполнения (на глубину, через все и др)</param>
        /// <param name="length">Глубина выдавливания</param>
        /// <returns>Интерфейс сущности выреза выдавливанием</returns>        
        public ksEntity CreateCutExtrusion(ksEntity sketch, short directionType, short endType, double length)
        {
            ksEntity cutExtrusion = (ksEntity)_part.NewEntity((short)Obj3dType.o3d_cutExtrusion);
            
            ksCutExtrusionDefinition cutExtrDef = (ksCutExtrusionDefinition)cutExtrusion.GetDefinition();
            if (cutExtrDef != null)
            {
                cutExtrDef.SetSketch(sketch);
                cutExtrDef.directionType = directionType;
                cutExtrDef.SetSideParam(true,
                                        endType,
                                        length, 
                                        0, 
                                        false);
                cutExtrDef.SetSideParam(false,	
                                        endType,	
                                        length,
                                        0,
                                        false);
                cutExtrDef.SetThinParam(false, 0, 0, 0);
            }

            return cutExtrusion;
        }

        /// <summary>
        /// Создание операции по сечениям
        /// </summary>
        /// <param name="sketches">Коллекция эскизов, через которые будет выполнена операция</param>
        /// <returns>Интерфейс сущности операции по сечениям</returns>
        public ksEntity CreateBaseLoft(ksEntityCollection sketches)
        {
            ksEntity loftEntity = (ksEntity)_part.NewEntity((short)Obj3dType.o3d_baseLoft);
            ksBaseLoftDefinition loftDef = (ksBaseLoftDefinition)loftEntity.GetDefinition();

            ksEntityCollection sketchCollection = loftDef.Sketchs();
            int count = sketches.GetCount();
            for (int i = 0; i < count; i++ )
            {
                sketchCollection.Add(sketches.GetByIndex(i));
            }   
            return loftEntity;
        }

        /// <summary>
        /// Создание операции выреза по сечениям
        /// </summary>
        /// <param name="sketches">Коллекция эскизов, через которые будет выполнена операция</param>
        /// <returns>Интерфейс сущности операции выреза по сечениям</returns>
        public ksEntity CreateCutLoft(ksEntityCollection sketches)
        {
            ksEntity loftEntity = (ksEntity)_part.NewEntity((short)Obj3dType.o3d_cutLoft);
            ksCutLoftDefinition loftDef = (ksCutLoftDefinition)loftEntity.GetDefinition();

            ksEntityCollection sketchCollection = loftDef.Sketchs();
            int count = sketches.GetCount();
            for (int i = 0; i < count; i++)
            {
                sketchCollection.Add(sketches.GetByIndex(i));
            }
            return loftEntity;
        }

        /// <summary>
        /// Создание фаски
        /// </summary>
        /// <param name="edge">Грань</param>
        /// <param name="isFirstDirection">Выбор первого или второго направления</param>
        /// <param name="dist1">Длина первого катета фаски</param>
        /// <param name="dist2">Длина второго катета фаски</param>
        /// <returns>Интерфейс сущности фаски</returns>
        public ksEntity CreateChamfer(ksEntity edge, bool isFirstDirection, double dist1, double dist2)
        {
            ksEntity entityChamfer = (ksEntity)_part.NewEntity((short)Obj3dType.o3d_chamfer);
            ksChamferDefinition ChamferDef = (ksChamferDefinition)entityChamfer.GetDefinition();            
            
            ChamferDef.tangent = false; //не продолжать по касательным ребрам
            ChamferDef.SetChamferParam(isFirstDirection, dist1, dist2);
            ksEntityCollection arr = (ksEntityCollection)ChamferDef.array();	// динамический массив объектов
                
            arr.Add(edge);
            return entityChamfer;
        }

    #endregion

        /// <summary>
        /// Нахождение плоскости по точке
        /// </summary>
        /// <param name="point">точка в трехмерном пространстве</param>
        /// <returns>Интерфейс сущности найденой плоскости</returns>
        public ksEntity GetPlaneByPoint(Point3D point)
        {
            ksEntityCollection allPlanes = (ksEntityCollection)_part.EntityCollection((short)Obj3dType.o3d_face);
            allPlanes.SelectByPoint(point.X, point.Y, point.Z);
            return allPlanes.GetByIndex(0);
        }

        /// <summary>
        /// Нахождение грани по точке
        /// </summary>
        /// <param name="point">точка в трехмерном пространстве</param>
        /// <returns>Интерфейс сущности найденной плоскости</returns>
        public ksEntity GetEdgeByPoint(Point3D point)
        {
            ksEntityCollection allEdges = (ksEntityCollection)_part.EntityCollection((short)Obj3dType.o3d_edge);
            allEdges.SelectByPoint(point.X, point.Y, point.Z);
            return allEdges.GetByIndex(0);
        }

        /// <summary>
        /// Создание смещенной плоскости
        /// </summary>
        /// <param name="basePlane">Опорная плоскость</param>
        /// <param name="offset">Смещение от опорной плоскости</param>
        /// <param name="isForwardDirection">Флаг проверки прямого направления</param>
        /// <returns>Интерфейс сущности созданной плоскости</returns>
        public ksEntity CreateOffsetPlane(ksEntity basePlane, double offset, bool isForwardDirection)
        {
            ksEntity offsetPlane = (ksEntity)_part.NewEntity((short)Obj3dType.o3d_planeOffset);
            PlaneOffsetDefinition offsDef = (PlaneOffsetDefinition)offsetPlane.GetDefinition();

            offsDef.SetPlane(basePlane);
            offsDef.offset = offset;
            offsDef.direction = isForwardDirection;
            offsetPlane.hidden = true;
            offsetPlane.Create();

            return offsetPlane;
        }
    }

}
