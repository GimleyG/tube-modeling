﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KompasLibrary.KompasAPI
{
    /// <summary>
    /// Точка в двумерном пространстве, тип координат Double
    /// </summary>
    public struct PointDouble
    {
        /// <summary>
        /// координата х точки
        /// </summary>
        public double X
        {
            get;
            private set;
        }

        /// <summary>
        /// координата у точки
        /// </summary>
        public double Y
        {
            get;
            private set;
        }

        /// <summary>
        /// конструктор создания объекта точки
        /// </summary>
        /// <param name="x">координата х</param>
        /// <param name="y">координата у</param>
        public PointDouble(double x, double y)
            : this()
        {
            X = x;
            Y = y;
        }
    }

    /// <summary>
    /// Точка в трехмерном пространстве, тип координат Double
    /// </summary>
    public struct Point3D
    {
        /// <summary>
        /// Автосвойство для координаты х точки
        /// </summary>
        public double X
        {
            get;
            private set;
        }

        /// <summary>
        /// Автосвойство для координаты у точки
        /// </summary>
        public double Y
        {
            get;
            private set;
        }

        /// <summary>
        /// Автосвойство для доступа к координате Z точки
        /// </summary>
        public double Z
        {
            get;
            private set;
        }

        /// <summary>
        /// конструктор для создания объекта точки
        /// </summary>
        /// <param name="x">координата х</param>
        /// <param name="y">координата у</param>
        /// <param name="z">координата z</param>
        public Point3D(double x, double y, double z)
            : this()
        {
            X = x;
            Y = y;
            Z = z;
        }
    }
}
