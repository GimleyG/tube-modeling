﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Kompas6API5;
using Microsoft.Win32;
using System.Windows.Forms;

using KompasLibrary.KompasAPI;
using KompasLibrary.Model.Interfaces;
using KompasLibrary.Model;
using KompasLibrary.Controller;

namespace KompasLibrary
{
    public class LibraryEntry
    {
        /// <summary>
        /// Экспортная функция для передачи имени библиотеки
        /// </summary>
        /// <returns>Имя библиотеки, которое будет отображаться в менеджере библиотек Компаса</returns>
        [return: MarshalAs(UnmanagedType.BStr)]
        public string GetLibraryName()
        {
            return "Построение трубы для бурильной установки";
        }
        
        /// <summary>
        /// Экспортная функция, выполняющаяся при запуске библиотеки
        /// </summary>
        /// <param name="command">Номер выполняемой команды</param>
        /// <param name="mode">Переданные флаги команды</param>
        /// <param name="kompas_">Ссылка на главный интерфейс API Компаса</param>
        public void ExternalRunCommand([In] short command, [In] short mode, [In, MarshalAs(UnmanagedType.IDispatch)] object kompas_)
        {
            KompasWrapper kompasAPI = new KompasWrapper((KompasObject)kompas_);
            
            ITubeModel model = new TubeModel(kompasAPI);
            
            ControllerGUI controller = new ControllerGUI(model, kompasAPI);
            controller.Start();
            controller.Stop();
           
            kompasAPI.Dispose();
        }

        #region COM Registration
		// Эта функция выполняется при регистрации класса для COM
		// Она добавляет в ветку реестра компонента раздел Kompas_Library,
		// который сигнализирует о том, что класс является приложением Компас,
		// а также заменяет имя InprocServer32 на полное, с указанием пути.
		// Все это делается для того, чтобы иметь возможность подключить
		// библиотеку на вкладке ActiveX.
		[ComRegisterFunction]
		public static void RegisterKompasLib(Type t)
		{
			try
			{
				RegistryKey regKey = Registry.LocalMachine;
				string keyName = @"SOFTWARE\Classes\CLSID\{" + t.GUID.ToString() + "}";
				regKey = regKey.OpenSubKey(keyName, true);
				regKey.CreateSubKey("Kompas_Library");
				regKey = regKey.OpenSubKey("InprocServer32", true);
				regKey.SetValue(null, System.Environment.GetFolderPath(Environment.SpecialFolder.System) + @"\mscoree.dll");
				regKey.Close();
			}
			catch (Exception ex)
			{
				MessageBox.Show(string.Format("При регистрации класса для COM-Interop произошла ошибка:\n{0}", ex));
			}
		}
		
		// Эта функция удаляет раздел Kompas_Library из реестра
		[ComUnregisterFunction]
		public static void UnregisterKompasLib(Type t)
		{
			RegistryKey regKey = Registry.LocalMachine;
			string keyName = @"SOFTWARE\Classes\CLSID\{" + t.GUID.ToString() + "}";
			RegistryKey subKey = regKey.OpenSubKey(keyName, true);
			subKey.DeleteSubKey("Kompas_Library");
			subKey.Close();
		}
		#endregion
	}

    
    
}
