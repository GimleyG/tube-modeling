﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using KompasLibrary.Model.Types;

namespace KompasLibrary.Model.Constants
{
    /// <summary>
    /// Константы резьбовых соединений
    /// </summary>
    internal struct ThreadConstants
    {
        /// <summary>
        /// Список названий резьбовых соединений
        /// </summary>
        public static List<String> ThreadTypeNames
        {
            get
            {
                List<String> res = new List<String>();
                res.Add("Z30");
                res.Add("Z35");
                res.Add("Z38");
                res.Add("Z44");
                res.Add("Z133");
                res.Add("Z140");
                res.Add("Z147");
                res.Add("Z149");
                return res;
            }
        }

        /// <summary>
        /// Тип резьбового соединения Z30
        /// </summary>
        public static ThreadType Z30
        {
            get
            {
                ThreadType thread = new ThreadType();
                thread.Name = "Z30";
                thread.Dl = 30.226;
                thread.Ds = 25.476;
                thread.K = (double)1 / 8;
                thread.Lpc = 38;
                thread.Qc = 30.582;
                thread.Qs = 27.742;
                thread.Lbc = 54;
                thread.L = 41;
                thread.Dt = 34;
                thread.Ofs = 15.88;
                return thread;
            }
        }

        /// <summary>
        /// Тип резьбового соединения Z35
        /// </summary>
        public static ThreadType Z35
        {
            get
            {
                ThreadType thread = new ThreadType();
                thread.Name = "Z35";
                thread.Dl = 35.357;
                thread.Ds = 29.857;
                thread.K = (double)1 / 8;
                thread.Lpc = 44;
                thread.Qc = 35.712;
                thread.Qs = 32.873;
                thread.Lbc = 60;
                thread.L = 47;
                thread.Dt = 40;
                thread.Ofs = 15.88;
                return thread;
            }
        }

        /// <summary>
        /// Тип резьбового соединения Z38
        /// </summary>
        public static ThreadType Z38
        {
            get
            {
                ThreadType thread = new ThreadType();
                thread.Name = "Z38";
                thread.Dl = 38.557;
                thread.Ds = 33.057;
                thread.K = (double)1 / 8;
                thread.Lpc = 44;
                thread.Qc = 38.913;
                thread.Qs = 36.073;
                thread.Lbc = 60;
                thread.L = 47;
                thread.Dt = 44.5;
                thread.Ofs = 15.88;
                return thread;
            }
        }

        /// <summary>
        /// Тип резьбового соединения Z44
        /// </summary>
        public static ThreadType Z44
        {
            get
            {
                ThreadType thread = new ThreadType();
                thread.Name = "Z44";
                thread.Dl = 44.094;
                thread.Ds = 38.594;
                thread.K = (double)1 / 8;
                thread.Lpc = 44;
                thread.Qc = 44.475;
                thread.Qs = 41.611;
                thread.Lbc = 60;
                thread.L = 47;
                thread.Dt = 52.5;
                thread.Ofs = 15.88;
                return thread;
            }
        }

        /// <summary>
        /// Тип резьбового соединения Z133
        /// </summary>
        public static ThreadType Z133
        {
            get
            {
                ThreadType thread = new ThreadType();
                thread.Name = "Z133";
                thread.Dl = 133.35;
                thread.Ds = 114.35;
                thread.K = (double)1 / 6;
                thread.Lpc = 114;
                thread.Qc = 134.935;
                thread.Qs = 128.074;
                thread.Lbc = 130;
                thread.L = 117;
                thread.Dt = 154;
                thread.Ofs = 15.88;
                return thread;
            }
        }

        /// <summary>
        /// Тип резьбового соединения Z140
        /// </summary>
        public static ThreadType Z140
        {
            get
            {
                ThreadType thread = new ThreadType();
                thread.Name = "Z140";
                thread.Dl = 140.208;
                thread.Ds = 110.208;
                thread.K = (double)1 / 4;
                thread.Lpc = 120;
                thread.Qc = 141.681;
                thread.Qs = 133.629;
                thread.Lbc = 136;
                thread.L = 123;
                thread.Dt = 165.4;
                thread.Ofs = 15.88;
                return thread;
            }
        }

        /// <summary>
        /// Тип резьбового соединения Z147
        /// </summary>
        public static ThreadType Z147
        {
            get
            {
                ThreadType thread = new ThreadType();
                thread.Name = "Z147";
                thread.Dl = 147.955;
                thread.Ds = 126.738;
                thread.K = (double)1 / 6;
                thread.Lpc = 127;
                thread.Qc = 150.016;
                thread.Qs = 141.363;
                thread.Lbc = 143;
                thread.L = 130;
                thread.Dt = 170.5;
                thread.Ofs = 15.88;
                return thread;
            }
        }

        /// <summary>
        /// Тип резьбового соединения Z149
        /// </summary>
        public static ThreadType Z149
        {
            get
            {
                ThreadType thread = new ThreadType();
                thread.Name = "Z149";
                thread.Dl = 149.25;
                thread.Ds = 117.5;
                thread.K = (double)1 / 4;
                thread.Lpc = 127;
                thread.Qc = 150.809;
                thread.Qs = 143.99;
                thread.Lbc = 143;
                thread.L = 130;
                thread.Dt = 185.5;
                thread.Ofs = 15.88;
                return thread;
            }
        }
    }

}
