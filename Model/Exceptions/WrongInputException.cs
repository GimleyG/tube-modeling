﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KompasLibrary.Model.Exceptions
{
    /// <summary>
    /// Исключение возникающее при неверном вводе параметров трубы
    /// </summary>
    public class WrongInputException : Exception
    {
        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public WrongInputException() : base() { }

        /// <summary>
        ///  Конструктор
        /// </summary>
        /// <param name="str">Сообщение об ошибке</param>
        public WrongInputException(string str) : base(str) { }

        /// <summary>
        /// Преведение объекта к строке
        /// </summary>
        /// <returns>Сообщение об ошибке</returns>
        public override string ToString()
        {
            return Message;
        }
    }

}
