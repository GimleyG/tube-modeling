﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KompasLibrary.Model.Interfaces
{
    /// <summary>
    /// Интерфейс модели трубы
    /// </summary>
    public interface ITubeModel
    {
        /// <summary>
        /// Инициализация модели
        /// </summary>
        void Initialize();

        /// <summary>
        /// Уничтожение модели
        /// </summary>
        void Dispose();

        /// <summary>
        /// Создание модели в Компасе
        /// </summary>
        void Create();

        /// <summary>
        /// Создание отчета по текущим параметрам модели
        /// </summary>
        /// <returns>Отчет в виде массива строк</returns>
        string[] CreateModelParametersReport();

        /// <summary>
        /// Регистрация слушателя модели
        /// </summary>
        /// <param name="obs">Слушатель</param>
        void RegisterObserver(ITubeModelObserver obs);

        /// <summary>
        /// Отписка слушателя модели
        /// </summary>
        /// <param name="obs">Слушатель</param>
        void DeleteObserver(ITubeModelObserver obs);

        /// <summary>
        /// Свойство получения сообщения об ошибке ввода
        /// </summary>
        string ErrorReport { get; }

        /// <summary>
        /// Свойство получения параметров модели
        /// </summary>
        ITubeParameters Parameters { get; }
    }
}
