﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KompasLibrary.Model.Interfaces
{
    /// <summary>
    /// Интерфейс слушателя модели
    /// </summary>
    public interface ITubeModelObserver
    {
        /// <summary>
        /// Вызывается при задании настроек по умолчанию
        /// </summary>
        void SetDefaultSettings();

        /// <summary>
        /// Вызывается при изменении типа трубы
        /// </summary>
        void ChangedTubeType();

        /// <summary>
        /// Вызывается при изменении типа ключа
        /// </summary>
        void ChangedKeyType();

        /// <summary>
        /// Вызывается при изменении параметров трубы
        /// </summary>
        void ChangedSizesOfTubeElements();

        /// <summary>
        /// Вызывается если поменялась необходимость в построении ступени
        /// </summary>
        void ChangedStairNeed();
    }
}
