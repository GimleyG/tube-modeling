﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using KompasLibrary.Model.Constants;
using KompasLibrary.Model.Types;

namespace KompasLibrary.Model.Interfaces
{
    /// <summary>
    /// Интерфейс параметров трубы
    /// </summary>
    public interface ITubeParameters
    {
        /// <summary>
        /// Свойство для получения списка названий типов резьб
        /// </summary>
        List<String> ThreadTypeNames { get; }

        /// <summary>
        /// Свойство для получения или задания типа трубы
        /// </summary>
        TubeType TypeOfTube { get; set; }

        /// <summary>
        /// Свойство для получения или задания типа ключа
        /// </summary>
        KeyType TypeOfKey { get; set; }

        /// <summary>
        /// Свойство для получения или задания типа резьбы на левой стороне по названию
        /// </summary>
        string LeftSideThreadName { get; set; }

        /// <summary>
        /// Свойство для получения или задания типа резьбы на правой стороне по названию
        /// </summary>
        string RightSideThreadName { get; set; }

        /// <summary>
        /// Свойство для получения или задания диаметра трубы
        /// </summary>
        double Diameter { get; set; }

        /// <summary>
        /// Свойство для получения или задания длины трубы
        /// </summary>
        double Length { get; set; }

        /// <summary>
        /// Свойство для получения или задания диаметра канала трубы
        /// </summary>
        double ChannelDiameter { get; set; }

        /// <summary>
        /// Свойство для получения или задания толщины стенки
        /// </summary>
        double WallThickness { get; set; }

        /// <summary>
        /// Свойство для получения или задания длины ключевого упора
        /// </summary>
        double KeyLength { get; set; }

        /// <summary>
        /// Свойство для получения или задания размера ключевого упора в поперечном сечении
        /// </summary>
        double KeySize { get; set; }

        /// <summary>
        /// Свойство для получения или задания смещения ключевого упора относительно торца
        /// </summary>
        double KeyOffset { get; set; }

        /// <summary>
        /// Свойство для получения флага необходимости построения ступени
        /// </summary>
        bool IsNeedStair { get; }

        /// <summary>
        /// Свойство для получения или задания диаметра ступени
        /// </summary>
        double StairDiameter { get; set; }

        /// <summary>
        /// Свойство для получения или задания длины ступени
        /// </summary>
        double StairLength { get; set; }

        /// <summary>
        /// Свойство для получения диапазона значений диаметра трубы
        /// </summary>
        ValueRange DiameterValueRange { get; }

        /// <summary>
        /// Свойство для получения диапазона значений длины трубы 
        /// </summary>
        ValueRange LengthValueRange { get; }

        /// <summary>
        /// Свойство для получения диапазона значений диаметра канала
        /// </summary>
        ValueRange ChannelDiameterValueRange { get; }

        /// <summary>
        /// Свойство для получения диапазона значений толщины стенки
        /// </summary>
        ValueRange WallThicknessValueRange { get; }

        /// <summary>
        /// Свойство для получения диапазона значений длины ключевого упора
        /// </summary>
        ValueRange KeyLengthValueRange { get; }

        /// <summary>
        /// Свойство для получения диапазона значений размера ключа в поперечном сечении
        /// </summary>
        ValueRange KeySizeValueRange { get; }

        /// <summary>
        /// Свойство для получения диапазона значений смещения ключевого упора относительно торца
        /// </summary>
        ValueRange KeyOffsetValueRange { get; }

        /// <summary>
        /// Свойство для получения диапазона значений диаметра ступени
        /// </summary>
        ValueRange StairDiameterValueRange { get; }

        /// <summary>
        /// Свойство для получения диапазона значений длины ступени
        /// </summary>
        ValueRange StairLengthValueRange { get; }
    }
}
