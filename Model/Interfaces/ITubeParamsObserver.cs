﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using KompasLibrary.Model.Types;

namespace KompasLibrary.Model.Interfaces
{
    /// <summary>
    /// Интерфейс слушателя параметров трубы
    /// </summary>
    public interface ITubeParamsObserver
    {
        /// <summary>
        /// Вызывается если есть изменения в параметре
        /// </summary>
        /// <param name="param">Имя изменненного параметра</param>
        void HasNewValueOfParameter(NameOfTubeParameter param);

        /// <summary>
        /// Вызывается если зафиксирована ошибка ввода
        /// </summary>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        void HasErrorWhileSetting(string errorMessage);
    }
}
