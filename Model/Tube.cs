﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kompas6API5;
using Kompas6Constants3D;

using KompasLibrary.KompasAPI;
using KompasLibrary.Model.Types;

namespace KompasLibrary.Model
{
    /// <summary>
    /// Базовый класс для построения с помощью API компаса модели трубы
    /// </summary>
    internal abstract class Tube
    {
        /// <summary>
        /// Параметры трубы
        /// </summary>
        protected TubeParameters _selfParams;

        /// <summary>
        /// Оберка компасовских API методов
        /// </summary>
        protected KompasWrapper _kompasAPI;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="api">Ссылка на объект обертки компаса</param>
        /// <param name="param">Ссылка на объект параметров</param>
        public Tube(KompasWrapper api, TubeParameters param)
        {
            _kompasAPI = api;
            _selfParams = param;
        }

        /// <summary>
        /// Сменить параметры 
        /// </summary>
        /// <param name="newParams">Новые параметры</param>
        public void ChangeParameters(TubeParameters newParams)
        {
            _selfParams = newParams;
        }

        /// <summary>
        /// Создать на основе параметров модель трубы
        /// </summary>
        public void Create()
        {
            _kompasAPI.Initialize();

            BuildMainPart();
            BuildLeftSide();
            BuildRightSide();

            BuildChannel();
        }

        /// <summary>
        /// Создание основной части
        /// </summary>
        private void BuildMainPart()
        {
            switch (_selfParams.TypeOfKey)
            {
                case KeyType.Round:
                    BuildWithRoundKey();
                    break;
                case KeyType.Hexahedron:
                    BuildWithHexKey();
                    break;
            }
            if (_selfParams.IsNeedStair)
                BuildStair();

        }

        /// <summary>
        /// Создание основной части с круговым ключом
        /// </summary>
        private void BuildWithRoundKey()
        {
            if (_kompasAPI.Part != null)
            {
                //создание эскиза
                ksEntity entitySketch = _kompasAPI.CreateCircleSketch(_kompasAPI.PlaneXOY,
                                                                      new PointDouble(0, 0),
                                                                      _selfParams.Diameter / 2
                                                                      );
                //базовая операция выдавливания
                ksEntity entityExtr = _kompasAPI.CreateBaseExtrusion(entitySketch,
                                                                    (short)Direction_Type.dtNormal,
                                                                    (short)End_Type.etBlind,
                                                                    _selfParams.Length
                                                                    );
                entityExtr.Create();

                double positionXStart = _selfParams.Diameter / 2;
                double positionYStart = _selfParams.Length - _selfParams.KeyOffset;
                double positionXEnd = 0.5 * (_selfParams.Diameter - _selfParams.KeySize);
                double positionYEnd = _selfParams.KeyLength;

                entitySketch = _kompasAPI.CreateRectangleSketch(_kompasAPI.PlaneXOZ,
                                                                new PointDouble(positionXStart, -positionYStart),
                                                                new PointDouble(-positionXEnd, positionYEnd)
                                                                );
                ksEntity entitySketchRev = _kompasAPI.CreateRectangleSketch(_kompasAPI.PlaneXOZ,
                                                                new PointDouble(-positionXStart, -positionYStart),
                                                                new PointDouble(positionXEnd, positionYEnd)
                                                                );

                //базовая операция выдавливания
                entityExtr = _kompasAPI.CreateCutExtrusion(entitySketch,
                                                                    (short)Direction_Type.dtBoth,
                                                                    (short)End_Type.etThroughAll,
                                                                    _selfParams.Length
                                                                    );
                entityExtr.Create();
                entityExtr = _kompasAPI.CreateCutExtrusion(entitySketchRev,
                                                                    (short)Direction_Type.dtBoth,
                                                                    (short)End_Type.etThroughAll,
                                                                    _selfParams.Length
                                                                    );
                entityExtr.Create();

                if (!_selfParams.IsNeedStair)
                {
                    //фаска на левом краю
                    ksEntity edge = _kompasAPI.GetEdgeByPoint(new Point3D(0, _selfParams.Diameter / 2, 0));
                    ksEntity chamfer = _kompasAPI.CreateChamfer(edge, true, 5, 5);
                    chamfer.Create();
                    //фаска на правом краю
                    edge = _kompasAPI.GetEdgeByPoint(new Point3D(0, _selfParams.Diameter / 2, _selfParams.Length));
                    chamfer = _kompasAPI.CreateChamfer(edge, true, 5, 5);
                    chamfer.Create();
                }
                else
                {
                    if (_selfParams.LeftSideThread.Dt < _selfParams.RightSideThread.Dt)
                    {
                        //фаска на правом краю
                        ksEntity edge = _kompasAPI.GetEdgeByPoint(new Point3D(0, _selfParams.Diameter / 2, _selfParams.Length));
                        ksEntity chamfer = _kompasAPI.CreateChamfer(edge, true, 5, 5);
                        chamfer.Create();
                    }
                    else
                    {
                        //фаска на левом краю
                        ksEntity edge = _kompasAPI.GetEdgeByPoint(new Point3D(0, _selfParams.Diameter / 2, 0));
                        ksEntity chamfer = _kompasAPI.CreateChamfer(edge, true, 5, 5);
                        chamfer.Create();
                    }
                }
            }
        }

        /// <summary>
        /// Создание основной части с ключом шестигранником
        /// </summary>
        private void BuildWithHexKey()
        {
            if (_kompasAPI.Part != null)
            {
                //выдавливание тела на основе эскиза окружности
                ksEntity sketchCircle = _kompasAPI.CreateCircleSketch(_kompasAPI.PlaneXOY,
                                                                      new PointDouble(0, 0),
                                                                      _selfParams.Diameter / 2
                                                                      );
                double offset = _selfParams.Length - _selfParams.KeyOffset - _selfParams.KeyLength;
                ksEntity entityExtr = _kompasAPI.CreateBaseExtrusion(sketchCircle,
                                                                (short)Direction_Type.dtNormal,
                                                                (short)End_Type.etBlind,
                                                                offset
                                                                );
                entityExtr.Create();

                //выдавливание шестиугольника на обратной стороне стержня
                ksEntity backPlane = _kompasAPI.GetPlaneByPoint(new Point3D(0, 0, offset));
                ksEntity sketchHex = _kompasAPI.CreateHexahedronSketch(backPlane,
                                                                new PointDouble(0, 0),
                                                                _selfParams.KeySize / 2
                                                                );
                entityExtr = _kompasAPI.CreateBaseExtrusion(sketchHex,
                                                            (short)Direction_Type.dtNormal,
                                                            (short)End_Type.etBlind,
                                                            _selfParams.KeyLength
                                                            );
                entityExtr.Create();

                //выдавливаем тело на основе эскиза окружности 
                double positionZ = offset + _selfParams.KeyLength;
                backPlane = _kompasAPI.GetPlaneByPoint(new Point3D(0, 0, positionZ));
                sketchCircle = _kompasAPI.CreateCircleSketch(backPlane,
                                                                new PointDouble(0, 0),
                                                                _selfParams.Diameter / 2
                                                                );

                entityExtr = _kompasAPI.CreateBaseExtrusion(sketchCircle,
                                                                (short)Direction_Type.dtNormal,
                                                                (short)End_Type.etBlind,
                                                                _selfParams.KeyOffset
                                                                );
                entityExtr.Create();

                if (!_selfParams.IsNeedStair)
                {
                    //фаска на левом краю
                    ksEntity edge = _kompasAPI.GetEdgeByPoint(new Point3D(0, _selfParams.Diameter / 2, 0));
                    ksEntity chamfer = _kompasAPI.CreateChamfer(edge, true, 5, 5);
                    chamfer.Create();
                    //фаска на правом краю
                    edge = _kompasAPI.GetEdgeByPoint(new Point3D(0, _selfParams.Diameter / 2, _selfParams.Length));
                    chamfer = _kompasAPI.CreateChamfer(edge, true, 5, 5);
                    chamfer.Create();
                }
                else
                {
                    if (_selfParams.LeftSideThread.Dt < _selfParams.RightSideThread.Dt)
                    {
                        //фаска на правом краю
                        ksEntity edge = _kompasAPI.GetEdgeByPoint(new Point3D(0, _selfParams.Diameter / 2, _selfParams.Length));
                        ksEntity chamfer = _kompasAPI.CreateChamfer(edge, true, 5, 5);
                        chamfer.Create();
                    }
                    else
                    {
                        //фаска на левом краю
                        ksEntity edge = _kompasAPI.GetEdgeByPoint(new Point3D(0, _selfParams.Diameter / 2, 0));
                        ksEntity chamfer = _kompasAPI.CreateChamfer(edge, true, 5, 5);
                        chamfer.Create();
                    }
                }
            }
        }

        /// <summary>
        /// Создание ступени
        /// </summary>
        private void BuildStair()
        {
            ThreadType thread;
            ksEntity basePlane;
            short directionType;
            double stairZCoord, stairChamferZCoord;
            if (_selfParams.LeftSideThread.Dt < _selfParams.RightSideThread.Dt)
            {
                thread = _selfParams.LeftSideThread;
                basePlane = _kompasAPI.PlaneXOY;
                directionType = (short)Direction_Type.dtReverse;
                stairZCoord = _selfParams.StairLength;
                stairChamferZCoord = 0;
            }
            else
            {
                thread = _selfParams.RightSideThread;
                basePlane = _kompasAPI.GetPlaneByPoint(new Point3D(0, 0, _selfParams.Length));
                directionType = (short)Direction_Type.dtNormal;
                stairZCoord = _selfParams.Length - _selfParams.StairLength;
                stairChamferZCoord = _selfParams.StairLength;
            }
            ksEntity ringSketch = _kompasAPI.CreateRingSketch(basePlane,
                                                              new PointDouble(0, 0),
                                                              _selfParams.Diameter / 2,
                                                              (_selfParams.StairDiameter) / 2
                                                              );
            ksEntity extrusion = _kompasAPI.CreateCutExtrusion(ringSketch,
                                                                directionType,
                                                                (short)End_Type.etBlind,
                                                                _selfParams.StairLength
                                                                );
            extrusion.Create();

            ksEntity stairEdge = _kompasAPI.GetEdgeByPoint(new Point3D(0, _selfParams.Diameter / 2, stairZCoord));
            double dist = (_selfParams.Diameter - _selfParams.StairDiameter) / 2;
            ksEntity stairChamfer = _kompasAPI.CreateChamfer(stairEdge,
                                                            true,
                                                            dist,
                                                            dist
                                                            );
            stairChamfer.Create();

            //фаска 
            ksEntity edge = _kompasAPI.GetEdgeByPoint(new Point3D(0, _selfParams.StairDiameter / 2, stairChamferZCoord));
            ksEntity chamfer = _kompasAPI.CreateChamfer(edge, true, 5, 5);
            chamfer.Create();

        }

        /// <summary>
        /// Создание левого резьбового соединения 
        /// </summary>
        protected abstract void BuildLeftSide();

        /// <summary>
        /// Создание правого резьбового соединения
        /// </summary>
        protected abstract void BuildRightSide();

        /// <summary>
        /// Дополнительные построения после создания канала
        /// </summary>
        protected abstract void AdditionalBuildAfterChannelCreation();

        /// <summary>
        /// Создание канала
        /// </summary>
        private void BuildChannel()
        {
            // создадим новый эскиз
            ksEntity entitySketch3 = _kompasAPI.CreateCircleSketch(_kompasAPI.PlaneXOY,
                                                                   new PointDouble(0, 0),
                                                                   _selfParams.ChannelDiameter / 2
                                                                   );
            if (entitySketch3 != null)
            {
                // вырежим выдавливанием
                ksEntity entityCutExtr = _kompasAPI.CreateCutExtrusion(entitySketch3,
                                                                   (short)Direction_Type.dtBoth,
                                                                   (short)End_Type.etThroughAll,
                                                                   _selfParams.Length
                                                                    );
                entityCutExtr.Create();
            }
            AdditionalBuildAfterChannelCreation();
        }

    }

    /// <summary>
    /// Класс для построения с помощью API компаса модели стандартной трубы
    /// </summary>
    internal class StandartTube : Tube
    {

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="kompasAPI">Ссылка на объект компасовской обертки</param>
        /// <param name="param">Ссылка на объект параметров</param>
        public StandartTube(KompasWrapper kompasAPI, TubeParameters param) :
            base(kompasAPI, param)
        {
        }

        protected override void BuildLeftSide()
        {
            if (_kompasAPI.Part != null)
            {
                //создание эскиза
                ksEntity bigConeBaseSketch = _kompasAPI.CreateCircleSketch(_kompasAPI.PlaneXOY,
                                                                      new PointDouble(0, 0),
                                                                      _selfParams.LeftSideThread.Dl / 2
                                                                      );
                ksEntity offsetPlane = _kompasAPI.CreateOffsetPlane(_kompasAPI.PlaneXOY,
                                                                    _selfParams.LeftSideThread.Lpc,
                                                                    false
                                                                    );
                ksEntity smallConeBaseSketch = _kompasAPI.CreateCircleSketch(offsetPlane,
                                                                      new PointDouble(0, 0),
                                                                      _selfParams.LeftSideThread.Ds / 2
                                                                      );
                ksEntityCollection sketchesForLoft = _kompasAPI.BlankEntityCollection;
                sketchesForLoft.Add(bigConeBaseSketch);
                sketchesForLoft.Add(smallConeBaseSketch);

                ksEntity baseLoft = _kompasAPI.CreateBaseLoft(sketchesForLoft);
                baseLoft.Create();

                //фаска на малом краю конуса
                double coordY = _selfParams.LeftSideThread.Ds / 2;
                ksEntity edge = _kompasAPI.GetEdgeByPoint(new Point3D(0, coordY, -_selfParams.LeftSideThread.Lpc));
                ksEntity chamfer = _kompasAPI.CreateChamfer(edge, true, 2, 2 * Math.Tan(Math.PI / 3));
                chamfer.Create();
            }
        }

        protected override void BuildRightSide()
        {
            if (_kompasAPI.Part != null)
            {
                //малая конусная выточка
                ksEntity backPlane = _kompasAPI.GetPlaneByPoint(new Point3D(0, 0, _selfParams.Length));
                ksEntity bigConeBaseSketch = _kompasAPI.CreateCircleSketch(backPlane,
                                                                           new PointDouble(0, 0),
                                                                           _selfParams.RightSideThread.Qc / 2
                                                                            );
                ksEntity offsetPlane = _kompasAPI.CreateOffsetPlane(backPlane,
                                                                    _selfParams.RightSideThread.Ofs,
                                                                    false
                                                                    );
                double smallConeBaseDiameter = _selfParams.RightSideThread.Qc - _selfParams.RightSideThread.Ofs * _selfParams.RightSideThread.K;
                ksEntity smallConeBaseSketch = _kompasAPI.CreateCircleSketch(offsetPlane,
                                                                            new PointDouble(0, 0),
                                                                            smallConeBaseDiameter / 2
                                                                            );
                ksEntityCollection sketchesForLoft = _kompasAPI.BlankEntityCollection;
                sketchesForLoft.Add(bigConeBaseSketch);
                sketchesForLoft.Add(smallConeBaseSketch);

                ksEntity baseLoft = _kompasAPI.CreateCutLoft(sketchesForLoft);
                baseLoft.Create();

                //большая конусная выточка
                bigConeBaseSketch = _kompasAPI.CreateCircleSketch(backPlane,
                                                                  new PointDouble(0, 0),
                                                                  _selfParams.RightSideThread.Qs / 2
                                                                  );
                offsetPlane = _kompasAPI.CreateOffsetPlane(backPlane,
                                                           _selfParams.RightSideThread.Lbc,
                                                           false
                                                           );
                smallConeBaseDiameter = _selfParams.RightSideThread.Qs - _selfParams.RightSideThread.Lbc * _selfParams.RightSideThread.K;
                smallConeBaseSketch = _kompasAPI.CreateCircleSketch(offsetPlane,
                                                                    new PointDouble(0, 0),
                                                                    smallConeBaseDiameter / 2
                                                                    );
                sketchesForLoft.Clear();
                sketchesForLoft.Add(bigConeBaseSketch);
                sketchesForLoft.Add(smallConeBaseSketch);

                baseLoft = _kompasAPI.CreateCutLoft(sketchesForLoft);
                baseLoft.Create();

                //фаска между конусными выточками
                double coordY = 0.5 * (_selfParams.RightSideThread.Qs - _selfParams.RightSideThread.Ofs * _selfParams.RightSideThread.K);
                ksEntity edge = _kompasAPI.GetEdgeByPoint(new Point3D(0, coordY, _selfParams.Length - _selfParams.RightSideThread.Ofs));

                double dist1 = 0.5 * (_selfParams.RightSideThread.Qc - _selfParams.RightSideThread.Qs);
                double dist2 = dist1 * Math.Tan(Math.PI / 3 + Math.Atan(0.5 * _selfParams.RightSideThread.K));

                ksEntity chamfer = _kompasAPI.CreateChamfer(edge,
                                                            true,
                                                            dist1,
                                                            dist2
                                                            );
                chamfer.Create();
            }
        }

        protected override void AdditionalBuildAfterChannelCreation()
        {
            double coordY = _selfParams.ChannelDiameter / 2;
            double coordZ = _selfParams.Length - _selfParams.RightSideThread.Lbc;
            ksEntity edge = _kompasAPI.GetEdgeByPoint(new Point3D(0, coordY, coordZ));

            double dist1 = 0.5 * (_selfParams.RightSideThread.Qs - _selfParams.RightSideThread.Lbc * _selfParams.RightSideThread.K - _selfParams.ChannelDiameter);
            double dist2 = dist1 * Math.Tan(Math.PI / 3);

            ksEntity chamfer = _kompasAPI.CreateChamfer(edge, true, dist1, dist2);
            chamfer.Create();
        }
    }

    /// <summary>
    /// Класс для построения с помощью API компаса модели муфтового переводника
    /// </summary>
    internal class ConnectorMuf : Tube
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="kompasAPI">Ссылка на объект компасовской обертки</param>
        /// <param name="param">Ссылка на объект параметров</param>
        public ConnectorMuf(KompasWrapper kompasAPI, TubeParameters param) :
            base(kompasAPI, param)
        {
        }

        protected override void BuildLeftSide()
        {
            if (_kompasAPI.Part != null)
            {
                //малая конусная выточка
                ksEntity backPlane = _kompasAPI.PlaneXOY;
                ksEntity bigConeBaseSketch = _kompasAPI.CreateCircleSketch(backPlane,
                                                                           new PointDouble(0, 0),
                                                                           _selfParams.LeftSideThread.Qc / 2
                                                                            );
                ksEntity offsetPlane = _kompasAPI.CreateOffsetPlane(backPlane,
                                                                    _selfParams.LeftSideThread.Ofs,
                                                                    true
                                                                    );
                double smallConeBaseDiameter = _selfParams.LeftSideThread.Qc - _selfParams.LeftSideThread.Ofs * _selfParams.LeftSideThread.K;
                ksEntity smallConeBaseSketch = _kompasAPI.CreateCircleSketch(offsetPlane,
                                                                            new PointDouble(0, 0),
                                                                            smallConeBaseDiameter / 2
                                                                            );
                ksEntityCollection sketchesForLoft = _kompasAPI.BlankEntityCollection;
                sketchesForLoft.Add(bigConeBaseSketch);
                sketchesForLoft.Add(smallConeBaseSketch);

                ksEntity baseLoft = _kompasAPI.CreateCutLoft(sketchesForLoft);
                baseLoft.Create();

                //большая конусная выточка
                bigConeBaseSketch = _kompasAPI.CreateCircleSketch(backPlane,
                                                                  new PointDouble(0, 0),
                                                                  _selfParams.LeftSideThread.Qs / 2
                                                                  );
                offsetPlane = _kompasAPI.CreateOffsetPlane(backPlane,
                                                           _selfParams.LeftSideThread.Lbc,
                                                           true
                                                           );
                smallConeBaseDiameter = _selfParams.LeftSideThread.Qs - _selfParams.LeftSideThread.Lbc * _selfParams.LeftSideThread.K;
                smallConeBaseSketch = _kompasAPI.CreateCircleSketch(offsetPlane,
                                                                    new PointDouble(0, 0),
                                                                    smallConeBaseDiameter / 2
                                                                    );
                sketchesForLoft.Clear();
                sketchesForLoft.Add(bigConeBaseSketch);
                sketchesForLoft.Add(smallConeBaseSketch);

                baseLoft = _kompasAPI.CreateCutLoft(sketchesForLoft);
                baseLoft.Create();

                //фаска между конусными выточками
                double coordY = 0.5 * (_selfParams.LeftSideThread.Qs - _selfParams.LeftSideThread.Ofs * _selfParams.LeftSideThread.K);
                ksEntity edge = _kompasAPI.GetEdgeByPoint(new Point3D(0, coordY, _selfParams.LeftSideThread.Ofs));

                double dist1 = 0.5 * (_selfParams.LeftSideThread.Qc - _selfParams.LeftSideThread.Qs);
                double dist2 = dist1 * Math.Tan(Math.PI / 3 + Math.Atan(0.5 * _selfParams.LeftSideThread.K));

                ksEntity chamfer = _kompasAPI.CreateChamfer(edge,
                                                            true,
                                                            dist1,
                                                            dist2
                                                            );
                chamfer.Create();
            }
        }

        protected override void BuildRightSide()
        {
            if (_kompasAPI.Part != null)
            {
                //малая конусная выточка
                ksEntity backPlane = _kompasAPI.GetPlaneByPoint(new Point3D(0, 0, _selfParams.Length));
                ksEntity bigConeBaseSketch = _kompasAPI.CreateCircleSketch(backPlane,
                                                                           new PointDouble(0, 0),
                                                                           _selfParams.RightSideThread.Qc / 2
                                                                            );
                ksEntity offsetPlane = _kompasAPI.CreateOffsetPlane(backPlane,
                                                                    _selfParams.RightSideThread.Ofs,
                                                                    false
                                                                    );
                double smallConeBaseDiameter = _selfParams.RightSideThread.Qc - _selfParams.RightSideThread.Ofs * _selfParams.RightSideThread.K;
                ksEntity smallConeBaseSketch = _kompasAPI.CreateCircleSketch(offsetPlane,
                                                                            new PointDouble(0, 0),
                                                                            smallConeBaseDiameter / 2
                                                                            );
                ksEntityCollection sketchesForLoft = _kompasAPI.BlankEntityCollection;
                sketchesForLoft.Add(bigConeBaseSketch);
                sketchesForLoft.Add(smallConeBaseSketch);

                ksEntity baseLoft = _kompasAPI.CreateCutLoft(sketchesForLoft);
                baseLoft.Create();

                //большая конусная выточка
                bigConeBaseSketch = _kompasAPI.CreateCircleSketch(backPlane,
                                                                  new PointDouble(0, 0),
                                                                  _selfParams.RightSideThread.Qs / 2
                                                                  );
                offsetPlane = _kompasAPI.CreateOffsetPlane(backPlane,
                                                           _selfParams.RightSideThread.Lbc,
                                                           false
                                                           );
                smallConeBaseDiameter = _selfParams.RightSideThread.Qs - _selfParams.RightSideThread.Lbc * _selfParams.RightSideThread.K;
                smallConeBaseSketch = _kompasAPI.CreateCircleSketch(offsetPlane,
                                                                    new PointDouble(0, 0),
                                                                    smallConeBaseDiameter / 2
                                                                    );
                sketchesForLoft.Clear();
                sketchesForLoft.Add(bigConeBaseSketch);
                sketchesForLoft.Add(smallConeBaseSketch);

                baseLoft = _kompasAPI.CreateCutLoft(sketchesForLoft);
                baseLoft.Create();

                //фаска между конусными выточками
                double coordY = 0.5 * (_selfParams.RightSideThread.Qs - _selfParams.RightSideThread.Ofs * _selfParams.RightSideThread.K);
                ksEntity edge = _kompasAPI.GetEdgeByPoint(new Point3D(0, coordY, _selfParams.Length - _selfParams.RightSideThread.Ofs));

                double dist1 = 0.5 * (_selfParams.RightSideThread.Qc - _selfParams.RightSideThread.Qs);
                double dist2 = dist1 * Math.Tan(Math.PI / 3 + Math.Atan(0.5 * _selfParams.RightSideThread.K));

                ksEntity chamfer = _kompasAPI.CreateChamfer(edge,
                                                            true,
                                                            dist1,
                                                            dist2
                                                            );
                chamfer.Create();
            }
        }

        protected override void AdditionalBuildAfterChannelCreation()
        {
            //фаска с левой стороны
            double coordY = _selfParams.ChannelDiameter / 2;
            double coordZLeft = _selfParams.LeftSideThread.Lbc;
            ksEntity edgeLeft = _kompasAPI.GetEdgeByPoint(new Point3D(0, coordY, coordZLeft));

            ThreadType left = _selfParams.LeftSideThread;
            double dist1Left = 0.5 * (left.Qs - left.Lbc * left.K - _selfParams.ChannelDiameter);
            double dist2Left = dist1Left * Math.Tan(Math.PI / 3);

            ksEntity chamfer = _kompasAPI.CreateChamfer(edgeLeft, true, dist1Left, dist2Left);
            chamfer.Create();

            //фаска с правой стороны
            double coordZRight = _selfParams.Length - _selfParams.RightSideThread.Lbc;
            ksEntity edgeRight = _kompasAPI.GetEdgeByPoint(new Point3D(0, coordY, coordZRight));

            ThreadType right = _selfParams.RightSideThread;
            double dist1Right = 0.5 * (right.Qs - right.Lbc * right.K - _selfParams.ChannelDiameter);
            double dist2Right = dist1Right * Math.Tan(Math.PI / 3);

            chamfer = _kompasAPI.CreateChamfer(edgeRight, true, dist1Right, dist2Right);
            chamfer.Create();
        }
    }

    /// <summary>
    /// Класс для построения с помощью API компаса модели ниппельного переводника
    /// </summary>
    internal class ConnectorNip : Tube
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="kompasAPI">Ссылка на объект компасовской обертки</param>
        /// <param name="param">Ссылка на объект параметров</param>
        public ConnectorNip(KompasWrapper kompasAPI, TubeParameters param) :
            base(kompasAPI, param)
        {
        }

        protected override void BuildLeftSide()
        {
            if (_kompasAPI.Part != null)
            {
                //создание эскиза
                ksEntity bigConeBaseSketch = _kompasAPI.CreateCircleSketch(_kompasAPI.PlaneXOY,
                                                                      new PointDouble(0, 0),
                                                                      _selfParams.LeftSideThread.Dl / 2
                                                                      );
                ksEntity offsetPlane = _kompasAPI.CreateOffsetPlane(_kompasAPI.PlaneXOY,
                                                                    _selfParams.LeftSideThread.Lpc,
                                                                    false
                                                                    );
                ksEntity smallConeBaseSketch = _kompasAPI.CreateCircleSketch(offsetPlane,
                                                                      new PointDouble(0, 0),
                                                                      _selfParams.LeftSideThread.Ds / 2
                                                                      );
                ksEntityCollection sketchesForLoft = _kompasAPI.BlankEntityCollection;
                sketchesForLoft.Add(bigConeBaseSketch);
                sketchesForLoft.Add(smallConeBaseSketch);

                ksEntity baseLoft = _kompasAPI.CreateBaseLoft(sketchesForLoft);
                baseLoft.Create();

                //фаска на малом краю конуса
                double coordY = _selfParams.LeftSideThread.Ds / 2;
                ksEntity edge = _kompasAPI.GetEdgeByPoint(new Point3D(0, coordY, -_selfParams.LeftSideThread.Lpc));
                ksEntity chamfer = _kompasAPI.CreateChamfer(edge, true, 2, 2 * Math.Tan(Math.PI / 3));
                chamfer.Create();
            }
        }

        protected override void BuildRightSide()
        {
            if (_kompasAPI.Part != null)
            {
                //создание эскиза
                ksEntity backPlane = _kompasAPI.GetPlaneByPoint(new Point3D(0, 0, _selfParams.Length));
                ksEntity bigConeBaseSketch = _kompasAPI.CreateCircleSketch(backPlane,
                                                                      new PointDouble(0, 0),
                                                                      _selfParams.RightSideThread.Dl / 2
                                                                      );
                ksEntity offsetPlane = _kompasAPI.CreateOffsetPlane(_kompasAPI.PlaneXOY,
                                                                     _selfParams.Length + _selfParams.RightSideThread.Lpc,
                                                                    true
                                                                    );
                ksEntity smallConeBaseSketch = _kompasAPI.CreateCircleSketch(offsetPlane,
                                                                      new PointDouble(0, 0),
                                                                      _selfParams.RightSideThread.Ds / 2
                                                                      );
                ksEntityCollection sketchesForLoft = _kompasAPI.BlankEntityCollection;
                sketchesForLoft.Add(bigConeBaseSketch);
                sketchesForLoft.Add(smallConeBaseSketch);

                ksEntity baseLoft = _kompasAPI.CreateBaseLoft(sketchesForLoft);
                baseLoft.Create();

                //фаска на малом краю конуса
                double coordY = _selfParams.RightSideThread.Ds / 2;
                ksEntity edge = _kompasAPI.GetEdgeByPoint(new Point3D(0, coordY, _selfParams.Length + _selfParams.RightSideThread.Lpc));
                ksEntity chamfer = _kompasAPI.CreateChamfer(edge, true, 2, 2 * Math.Tan(Math.PI / 3));
                chamfer.Create();
            }
        }

        protected override void AdditionalBuildAfterChannelCreation()
        {
            //do nothing
        }
    }
}
