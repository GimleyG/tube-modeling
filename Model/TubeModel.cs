﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

using KompasLibrary.Model.Types;
using KompasLibrary.Model.Interfaces;
using KompasLibrary.KompasAPI;

namespace KompasLibrary.Model
{
    /// <summary>
    /// Класс управления параметрами и созданием модели трубы
    /// </summary>
    public class TubeModel : ITubeParamsObserver, ITubeModel
    {
        /// <summary>
        /// Объект класса построния трубы
        /// </summary>
        private Tube _tubeInstance;

        /// <summary>
        /// Объект класса параметров
        /// </summary>
        private TubeParameters _params;

        /// <summary>
        /// Объект класса обертки методов API компаса
        /// </summary>
        private KompasWrapper _kompasAPI;

        /// <summary>
        /// Список слушателей данного класса
        /// </summary>
        private List<ITubeModelObserver> _observers;

        /// <summary>
        /// Сообщение об ошибке ввода. Может использоваться слушателями
        /// </summary>
        private string _errorReport;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="kompasAPI">Ссылка на класс обертку компаса </param>
        public TubeModel(KompasWrapper kompasAPI)
        {
            _tubeInstance = null;
            _kompasAPI = kompasAPI;
            _params = new TubeParameters();
            _params.RegisterObserver(this);
            _observers = new List<ITubeModelObserver>();
            _errorReport = "";
        }

        /// <summary>
        /// Инициализация модели
        /// </summary>
        public void Initialize() 
        {
            _kompasAPI.KompasObj.ksEnableTaskAccess(0);
            NotifyObservers(ModelObsNotificationType.SetToDefault);
        }

        /// <summary>
        /// Уничтожение модели
        /// </summary>
        public void Dispose()
        {
            _kompasAPI.KompasObj.ksEnableTaskAccess(1);
            _tubeInstance = null;
            _params = null;
            _kompasAPI = null;
            _observers = null;
        }

        /// <summary>
        /// Создание модели по заданным параметрам
        /// </summary>
        public void Create()
        {
            switch (_params.TypeOfTube)
            {
                case TubeType.Standart:
                    _tubeInstance = new StandartTube(_kompasAPI, _params);
                    break;
                case TubeType.ConnectorNip:
                    _tubeInstance = new ConnectorNip(_kompasAPI, _params);
                    break;
                case TubeType.ConnectorMuf:
                    _tubeInstance = new ConnectorMuf(_kompasAPI, _params);
                    break;
            }
            _tubeInstance.Create();
        }

        /// <summary>
        /// Регистрация слушателя
        /// </summary>
        /// <param name="obs">Слушатель</param>
        public void RegisterObserver(ITubeModelObserver obs)
        {
            _observers.Add(obs);
        }

        /// <summary>
        /// Удаление слушателя
        /// </summary>
        /// <param name="obs">Слушатель</param>
        public void DeleteObserver(ITubeModelObserver obs)
        {
            _observers.Remove(obs);
        }

        /// <summary>
        /// Создание многострочного отчета о значениях параметров модели
        /// </summary>
        /// <returns>Отчет о параметрах</returns>
        public string[] CreateModelParametersReport()
        {
            const int reportLength = 16;
            string[] report = new string[reportLength];
            report[0] = "Тип трубы:";
            switch(_params.TypeOfTube)
            {
                case TubeType.Standart:
                    report[1] = "\tСтандартная труба";
                    break;
                case TubeType.ConnectorNip:
                    report[1] = "\tПереводник ниппельный";
                    break;
                case TubeType.ConnectorMuf:
                    report[1] = "\tПереводник муфтовый";
                    break;
            }
            report[2] = "Тип ключа:";
            switch (_params.TypeOfKey)
            {
                case KeyType.Round:
                    report[3] = "\tКруговой";
                    break;
                case KeyType.Hexahedron:
                    report[3] = "\tШестигранник";
                    break;
            }
            report[4] = "Тип резьбы:";
            report[5] = "\tЛевый конец: " + _params.LeftSideThreadName;
            report[6] = "\tПравый конец: " + _params.RightSideThreadName;

            report[7] = "Диаметер: " + Convert.ToString(_params.Diameter);
            report[8] = "Длина: " + Convert.ToString(_params.Length);
            report[9] = "Диаметер канала: " + Convert.ToString(_params.ChannelDiameter);
            report[10] = "Толщина стенки: " + Convert.ToString(_params.WallThickness);

            report[11] = "Размер ключевого упора: " + Convert.ToString(_params.KeySize);
            report[12] = "Ширина ключевого упора: " + Convert.ToString(_params.KeyLength);
            report[13] = "Смещение ключевого упора от торца: " + Convert.ToString(_params.KeyOffset);

            report[14] = "Диаметр ступени: " + _params.StairDiameter;
            report[15] = "Длина ступени: " + _params.StairLength;
            
            return report; 
        }

        /// <summary>
        /// Свойство доступа к сообщению об ошибке ввода
        /// </summary>
        public string ErrorReport
        {
            get { return _errorReport; }
        }        
        
        /// <summary>
        /// Свойство для доступа к параметрам модели
        /// </summary>
        public ITubeParameters Parameters
        {
            get { return _params; }            
        }

        /// <summary>
        /// Метод оповещения слушателей о событии
        /// </summary>
        /// <param name="type">Тип события</param>
        private void NotifyObservers(ModelObsNotificationType type)
        {
            IEnumerator<ITubeModelObserver> enumer = _observers.GetEnumerator();
            for (enumer.Reset(); enumer.MoveNext(); )
            {
                switch(type)
                {
                    case ModelObsNotificationType.ChangedTubeType:
                        enumer.Current.ChangedTubeType();
                        break;
                    case ModelObsNotificationType.ChangedKeyType:
                        enumer.Current.ChangedKeyType();
                        break;
                    case ModelObsNotificationType.ChangedSizesOfTubeElements:
                        enumer.Current.ChangedSizesOfTubeElements();
                        break;
                    case ModelObsNotificationType.ChangedStairNeed:
                        enumer.Current.ChangedStairNeed();
                        break;
                    case ModelObsNotificationType.SetToDefault:
                        enumer.Current.SetDefaultSettings();
                        break;
                }
            }
        }              

        /// <summary>
        /// Действия при изменении объекта-параметров
        /// </summary>
        /// <param name="param">Измененный параметр</param>
        public void HasNewValueOfParameter(NameOfTubeParameter param)
        {
            switch (param)
            {
                case NameOfTubeParameter.TypeOfTube:
                    NotifyObservers(ModelObsNotificationType.ChangedTubeType);
                    break;
                case NameOfTubeParameter.TypeOfKey:
                    NotifyObservers(ModelObsNotificationType.ChangedKeyType);
                    break;
                case NameOfTubeParameter.RightSideThread:
                 case NameOfTubeParameter.LeftSideThread:
                    NotifyObservers(ModelObsNotificationType.ChangedStairNeed);
                    break;
                default:
                    NotifyObservers(ModelObsNotificationType.ChangedSizesOfTubeElements);
                    break;
            }
        }

        /// <summary>
        /// Действия при ошибке ввода параметра
        /// </summary>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        public void HasErrorWhileSetting(string errorMessage)
        {
            _errorReport = errorMessage;
        }       
    }    
}
