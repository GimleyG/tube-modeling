﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using KompasLibrary.Model.Exceptions;
using KompasLibrary.Model.Constants;
using KompasLibrary.Model.Types;
using KompasLibrary.Model.Interfaces;

namespace KompasLibrary.Model
{
    /// <summary>
    /// Класс для проверки и хранения параметров модели трубы
    /// </summary>
    public class TubeParameters : ITubeParameters
    {
        #region Поля
        /// <summary>
        /// Список слушателей
        /// </summary>
        List<ITubeParamsObserver> _observers;

        /// <summary>
        /// Тип трубы
        /// </summary>
        private TubeType _tubeType;

        /// <summary>
        /// Диаметр трубы
        /// </summary>
        private double _diameter;

        /// <summary>
        /// Длина трубы
        /// </summary>
        private double _length;

        /// <summary>
        /// Диаметр канала
        /// </summary>
        private double _channelDiameter;

        /// <summary>
        /// Тип резьбы для левого конца трубы
        /// </summary>
        private ThreadType _leftSide;

        /// <summary>
        /// Тип резьбы для правого конца трубы
        /// </summary>
        private ThreadType _rightSide;

        /// <summary>
        /// Тип ключа
        /// </summary>
        private KeyType _keyType;

        /// <summary>
        /// Ширина ключа
        /// </summary>
        private double _keyLength;

        /// <summary>
        /// Размер ключа
        /// </summary>
        private double _keySize;

        /// <summary>
        /// Смещение ключа от торца трубы
        /// </summary>
        private double _keyOffset;

        /// <summary>
        /// Флаг необходимости построения ступени
        /// </summary>
        private bool _needStair;

        /// <summary>
        /// Длина ступени
        /// </summary>
        private double _stairLength;

        /// <summary>
        /// Диаметр ступени
        /// </summary>
        private double _stairDiameter;

        /// <summary>
        /// Толщина стенки
        /// </summary>
        private double _thickness;

        #endregion

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public TubeParameters()
        {
            _observers = new List<ITubeParamsObserver>();

            _tubeType = TubeType.Standart;
            _keyType = KeyType.Round;

            _leftSide = ThreadConstants.Z44;
            _rightSide = ThreadConstants.Z44;

            _diameter = 70;
            _channelDiameter = 23;
            _length = 300;
            _thickness = 15;

            _keyLength = 22;
            _keyOffset = 70;
            _keySize = 50;

            _needStair = false;
            _stairLength = 0;
            _stairDiameter = 0;
        }

        #region Работа со слушателями

        /// <summary>
        /// Регистрация слушателя
        /// </summary>
        /// <param name="obs">Слушатель</param>
        public void RegisterObserver(ITubeParamsObserver obs)
        {
            _observers.Add(obs);
        }

        /// <summary>
        /// Удаление слушателя
        /// </summary>
        /// <param name="obs">Конкретный слушатель</param>
        public void DeleteObserver(ITubeParamsObserver obs)
        {
            _observers.Remove(obs);
        }

        /// <summary>
        /// Оповещение слушателей о присвоении нового значения параметру
        /// </summary>
        /// <param name="param">Имя параметра</param>
        private void NotifyObserversAboutSetsNewValue(NameOfTubeParameter param)
        {
            IEnumerator<ITubeParamsObserver> enumer = _observers.GetEnumerator();
            for (enumer.Reset(); enumer.MoveNext(); )
            {
                enumer.Current.HasNewValueOfParameter(param);
            }
        }

        /// <summary>
        /// Оповещение слушателей об ошибке ввода
        /// </summary>
        /// <param name="errMesg">Сообщение об ошибке</param>
        private void NotifyObserversAboutInputError(string errMesg)
        {
            IEnumerator<ITubeParamsObserver> enumer = _observers.GetEnumerator();
            for (enumer.Reset(); enumer.MoveNext(); )
            {
                enumer.Current.HasErrorWhileSetting(errMesg);
            }
        }

        #endregion

        #region Properties for accessing to parameters of model

        /// <summary>
        /// Свойство для получения названий всех используемых типов резьбовых соединений
        /// </summary>
        public List<String> ThreadTypeNames
        {
            get { return ThreadConstants.ThreadTypeNames; }
        }

        /// <summary>
        /// Свойство для досутпа к типу трубы
        /// </summary>
        public TubeType TypeOfTube
        {
            get { return _tubeType; }
            set
            {
                _tubeType = value;
                NotifyObserversAboutSetsNewValue(NameOfTubeParameter.TypeOfTube);
            }
        }

        /// <summary>
        /// Свойство для доступа к типу ключа
        /// </summary>
        public KeyType TypeOfKey
        {
            get { return _keyType; }
            set
            {
                _keyType = value;
                NotifyObserversAboutSetsNewValue(NameOfTubeParameter.TypeOfKey);
            }
        }

        /// <summary>
        /// Свойство для доступа к названию резьбового соединения на левом конце
        /// </summary>
        public string LeftSideThreadName
        {
            get { return _leftSide.Name; }
            set
            {
                _leftSide = GetThreadTypeByNameOfConstant(value);
                if (CheckThreads())
                    _needStair = false;
                else
                    _needStair = true;
                NotifyObserversAboutSetsNewValue(NameOfTubeParameter.LeftSideThread);
            }
        }

        /// <summary>
        /// Свойство доступа к значению резьбового соединения на левом конце
        /// </summary>
        internal ThreadType LeftSideThread
        {
            get { return _leftSide; }
        }

        /// <summary>
        /// Свойство для доступа к названию резьбового соединения на правом конце
        /// </summary>
        public string RightSideThreadName
        {
            get { return _rightSide.Name; }
            set
            {
                _rightSide = GetThreadTypeByNameOfConstant(value);
                
                if (CheckThreads())
                    _needStair = false;
                else
                    _needStair = true;
                NotifyObserversAboutSetsNewValue(NameOfTubeParameter.RightSideThread);
            }
        }

        /// <summary>
        /// Свойство доступа к значению резьбового соединения на правом конце
        /// </summary>
        internal ThreadType RightSideThread
        {
            get { return _rightSide; }
        }

        /// <summary>
        /// Возвращает тип резьбы по введенному имени контсанты
        /// </summary>
        /// <param name="constName">Имя константы</param>
        /// <returns>Тип резьбы</returns>
        private ThreadType GetThreadTypeByNameOfConstant(string constName)
        {
            ThreadType type = new ThreadType();
            switch (constName)
            {
                case "Z30":
                    type = ThreadConstants.Z30;
                    break;
                case "Z35":
                    type = ThreadConstants.Z35;
                    break;
                case "Z38":
                    type = ThreadConstants.Z38;
                    break;
                case "Z44":
                    type = ThreadConstants.Z44;
                    break;
                case "Z133":
                    type = ThreadConstants.Z133;
                    break;
                case "Z140":
                    type = ThreadConstants.Z140;
                    break;
                case "Z147":
                    type = ThreadConstants.Z147;
                    break;
                case "Z149":
                    type = ThreadConstants.Z149;
                    break;
            }
            return type;
        }

        /// <summary>
        /// Свойство доступа к диаметру трубы
        /// </summary>
        public double Diameter
        {
            get { return _diameter; }
            set
            {
                ValueRange range = DiameterValueRange;
                if (value <= range.Max && value >= range.Min)
                {
                    _diameter = value;
                    NotifyObserversAboutSetsNewValue(NameOfTubeParameter.Diameter);
                }
                else
                {
                    string errorMsg = "Допустимое значение диаметра лежит в пределах от " + range.Min + " до " + range.Max;
                    NotifyObserversAboutInputError(errorMsg);
                    var ex = CreateExceptionWithMessageAndData(errorMsg, NameOfTubeParameter.Diameter);                    
                    throw ex;
                }
            }
        }

        /// <summary>
        /// Создает исключение с заданным сообщением и данными в виде названия параметра
        /// </summary>
        /// <param name="message">Сообщение об ошибке</param>
        /// <param name="data">Название параметра</param>
        /// <returns>Объект созданного исключения</returns>
        private WrongInputException CreateExceptionWithMessageAndData(string message, NameOfTubeParameter data)
        {
            WrongInputException ex = new WrongInputException(message);
            ex.Data["Parameter"] = data;
            return ex;
        }

        /// <summary>
        /// Свойство доступа к длине трубы
        /// </summary>
        public double Length
        {
            get { return _length; }
            set
            {

                ValueRange range = LengthValueRange;
                if (value <= range.Max && value >= range.Min)
                {
                    _length = value;
                    NotifyObserversAboutSetsNewValue(NameOfTubeParameter.Length);
                }
                else
                {
                    string errorMsg = "Допустимое значение длины лежит в пределах от " + range.Min + " до " + range.Max;
                    
                    NotifyObserversAboutInputError(errorMsg);
                    var ex = CreateExceptionWithMessageAndData(errorMsg, NameOfTubeParameter.Length);
                    throw ex;
                }
            }
        }

        /// <summary>
        /// Свойство доступа к диаметру канала
        /// </summary>
        public double ChannelDiameter
        {
            get { return _channelDiameter; }
            set
            {
                ValueRange range = ChannelDiameterValueRange;
                if (value <= range.Max && value >= range.Min)
                {
                    _channelDiameter = value;
                    NotifyObserversAboutSetsNewValue(NameOfTubeParameter.ChannelDiameter);
                }
                else
                {
                    string errorMsg = "Допустимое значение диаметра канала лежит в пределах от " + range.Min + " до " + range.Max;
                   
                    NotifyObserversAboutInputError(errorMsg);
                    var ex = CreateExceptionWithMessageAndData(errorMsg, NameOfTubeParameter.ChannelDiameter);
                    throw ex;
                }
            }
        }

        /// <summary>
        /// Свойство доступа к толщине стенки
        /// </summary>
        public double WallThickness
        {
            get { return _thickness; }
            set
            {
                ValueRange range = WallThicknessValueRange;
                if (value <= range.Max && value >= range.Min)
                {
                    _thickness = value;
                    NotifyObserversAboutSetsNewValue(NameOfTubeParameter.WallThickness);
                }
                else
                {
                    string errorMsg = "Допустимое значение толщины стенки лежит в пределах от " + range.Min + " до " + range.Max;
                                        
                    NotifyObserversAboutInputError(errorMsg);
                    var ex = CreateExceptionWithMessageAndData(errorMsg, NameOfTubeParameter.WallThickness);
                    throw ex;
                }
            }
        }

        /// <summary>
        /// Свойство доступа к ширине ключа
        /// </summary>
        public double KeyLength
        {
            get { return _keyLength; }
            set
            {
                ValueRange range = KeyLengthValueRange;
                if (value <= range.Max && value >= range.Min)
                {
                    _keyLength = value;
                    NotifyObserversAboutSetsNewValue(NameOfTubeParameter.KeyLength);
                }
                else
                {
                    string errorMsg = "Допустимое значение ширины ключевого упора лежит в пределах от " + range.Min + " до " + range.Max;
                    NotifyObserversAboutInputError(errorMsg);
                    var ex = CreateExceptionWithMessageAndData(errorMsg, NameOfTubeParameter.KeyLength);
                    throw ex;
                }
            }
        }

        /// <summary>
        /// Свойство доступа к размеру ключа
        /// </summary>
        public double KeySize
        {
            get { return _keySize; }
            set
            {
                ValueRange range = KeySizeValueRange;
                if (value <= range.Max && value >= range.Min)
                {
                    _keySize = value;
                    NotifyObserversAboutSetsNewValue(NameOfTubeParameter.KeySize);
                }
                else
                {
                    string errorMsg = "Допустимое значение размера ключевого упора лежит в пределах от " + range.Min + " до " + range.Max;
                    NotifyObserversAboutInputError(errorMsg);
                    var ex = CreateExceptionWithMessageAndData(errorMsg, NameOfTubeParameter.KeySize);
                    throw ex;
                }
            }
        }

        /// <summary>
        /// Свойство доступа к смещению ключа от торца
        /// </summary>
        public double KeyOffset
        {
            get { return _keyOffset; }
            set
            {
                ValueRange range = KeyOffsetValueRange;
                if (value <= range.Max && value >= range.Min)
                {
                    _keyOffset = value;
                    NotifyObserversAboutSetsNewValue(NameOfTubeParameter.KeyOffset);
                }
                else
                {
                    string errorMsg = "Допустимое значение размера смещения лежит в пределах от " + range.Min + " до " + range.Max;
                    NotifyObserversAboutInputError(errorMsg);
                    var ex = CreateExceptionWithMessageAndData(errorMsg, NameOfTubeParameter.KeyOffset);
                    throw ex;
                }

            }
        }

        /// <summary>
        /// Свойство доступа к флагу необходимости построения ступени
        /// </summary>
        public bool IsNeedStair
        {
            get { return _needStair; }
        }

        /// <summary>
        /// Свойство доступа к диаметру ступени
        /// </summary>
        public double StairDiameter
        {
            get { return _stairDiameter; }
            set
            {
                ValueRange range = StairDiameterValueRange;
                if (value <= range.Max && value >= range.Min)
                {
                    _stairDiameter = value;
                    NotifyObserversAboutSetsNewValue(NameOfTubeParameter.StairDiameter);
                }
                else
                {
                    string errorMsg = "Допустимое значение диаметра ступени лежит в пределах от " + range.Min + " до " + range.Max;
                    NotifyObserversAboutInputError(errorMsg);
                    var ex = CreateExceptionWithMessageAndData(errorMsg, NameOfTubeParameter.StairDiameter);
                    throw ex;
                }
            }
        }

        /// <summary>
        /// Свойство доступа к длине ступени
        /// </summary>
        public double StairLength
        {
            get { return _stairLength; }
            set
            {
                ValueRange range = StairLengthValueRange;
                if (value <= range.Max && value >= range.Min)
                {
                    _stairLength = value;
                    NotifyObserversAboutSetsNewValue(NameOfTubeParameter.StairLength);
                }
                else
                {
                    string errorMsg = "Допустимое значение длины ступени лежит в пределах от " + range.Min + " до " + range.Max;
                    NotifyObserversAboutInputError(errorMsg);
                    var ex = CreateExceptionWithMessageAndData(errorMsg, NameOfTubeParameter.StairLength);
                    throw ex;
                }
            }
        }

        #endregion

        #region Диапазоны значений параметров

        /// <summary>
        /// Свойство для получения диапазона значений диаметра
        /// </summary>
        public ValueRange DiameterValueRange
        {            
            get
            {
                double inc = 14;
                ValueRange range = new ValueRange();
                if (_leftSide.Dt > _rightSide.Dt)
                {
                    range.Min = _leftSide.Dt + inc;
                    range.Max = (_leftSide.Dt + inc) * 1.2;
                }
                else
                {
                    range.Min = _rightSide.Dt + inc;
                    range.Max = (_rightSide.Dt + inc) * 1.2;
                }

                return range;
            }
        }

        /// <summary>
        /// Свойство для получения диапазона значений длины
        /// </summary>
        public ValueRange LengthValueRange
        {
            get
            {
                ValueRange range = new ValueRange();
                double value = _leftSide.Lpc + _rightSide.Lbc;
                range.Min = 2.5 * value;
                range.Max = 4 * value;
                return range;
            }
        }

        /// <summary>
        /// Свойство для получения диапазона значений диаметра канала
        /// </summary>
        public ValueRange ChannelDiameterValueRange
        {
            get
            {
                ValueRange range = new ValueRange();
                if (_leftSide.Dt > _rightSide.Dt)
                {
                    range.Min = 10;
                    range.Max = _rightSide.Qs - _rightSide.Lbc * _rightSide.K - 10;
                }
                else
                {
                    range.Min = 10;
                    range.Max = _leftSide.Qs - _leftSide.Lbc * _leftSide.K - 10;
                }
                return range;
            }

        }

        /// <summary>
        /// Свойство для получения диапазона значений толщины стенки
        /// </summary>
        public ValueRange WallThicknessValueRange
        {
            get
            {
                ValueRange range = new ValueRange();
                if (_leftSide.Dt > _rightSide.Dt)
                {
                    range.Min = 0.5 * (_rightSide.Qc - _channelDiameter);
                    range.Max = 0.5 * (_diameter - _channelDiameter);
                }
                else
                {
                    range.Min = 0.5 * (_leftSide.Qc - _channelDiameter);
                    range.Max = 0.5 * (_diameter - _channelDiameter);
                }
                return range;
            }

        }

        /// <summary>
        /// Свойство для получения диапазона значений ширины ключа
        /// </summary>
        public ValueRange KeyLengthValueRange
        {
            get
            {
                ValueRange range = new ValueRange();
                range.Min = 20;
                range.Max = 20 + 0.1 * _length ;
                return range;
            }

        }

        /// <summary>
        /// Свойство для получения диапазона значений размера ключа
        /// </summary>
        public ValueRange KeySizeValueRange
        {
            get
            {
                ValueRange range = new ValueRange();
                double minRange = (_leftSide.Dt > _rightSide.Dt) ? _leftSide.Qc : _rightSide.Qc;
                double maxRange = _diameter - 0.3*(_diameter - minRange);
                range.Min = minRange;
                range.Max = maxRange;
                return range;
            }

        }

        /// <summary>
        /// Свойство для получения диапазона значений смещения ключа
        /// </summary>
        public ValueRange KeyOffsetValueRange
        {
            get
            {
                ValueRange range = new ValueRange();
                range.Min = _rightSide.Lbc;
                range.Max = 1.5 * _rightSide.Lbc;
                return range;
            }
        }

        /// <summary>
        /// Свойство для получения диапазона значений диаметра ступени
        /// </summary>
        public ValueRange StairDiameterValueRange
        {
            get
            {
                ValueRange range = new ValueRange();
                range.Min = _thickness;
                range.Max = _diameter;
                return range;
            }

        }

        /// <summary>
        /// Свойство для получения диапазона значений длины ступени
        /// </summary>
        public ValueRange StairLengthValueRange
        {
            get
            {
                ValueRange range = new ValueRange();
                double minRange = (_leftSide.Dt < _rightSide.Dt) ? _leftSide.Lbc : _rightSide.Lbc;

                range.Min = 1.5 * minRange;
                range.Max = 0.5 * _length;
                return range;
            }
        }

        #endregion

        /// <summary>
        /// Проверка размеров резьбовых соединений чтобы узнать нужна ли ступень
        /// </summary>
        /// <returns>Флаг необходимости ступени</returns>
        private bool CheckThreads()
        {
            if (1.5 * _leftSide.Dt <= _rightSide.Dt)
            {
                _stairDiameter = _leftSide.Dt;
                _stairLength = _leftSide.L;
                return false;
            }
            else if (1.5 * _rightSide.Dt <= _leftSide.Dt)
            {
                _stairDiameter = _rightSide.Dt;
                _stairLength = _rightSide.L;
                return false;
            }
            _stairDiameter = 0;
            _stairLength = 0;
            return true;
        }
    }

}
