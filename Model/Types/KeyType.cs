﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KompasLibrary.Model.Types
{
    /// <summary>
    /// Названия типов ключей
    /// </summary>
    public enum KeyType
    {
        Round,
        Hexahedron
    }
}
