﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KompasLibrary.Model.Types
{
    /// <summary>
    /// Типы нотификации слушателей модели
    /// </summary>
    internal enum ModelObsNotificationType
    {
        ChangedTubeType,
        ChangedKeyType,
        ChangedSizesOfTubeElements,
        ChangedStairNeed,
        SetToDefault
    }

}
