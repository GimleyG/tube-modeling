﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KompasLibrary.Model.Types
{
    /// <summary>
    /// Названия параметров трубы
    /// </summary>
    public enum NameOfTubeParameter
    {
        TypeOfTube,
        TypeOfKey,

        LeftSideThread,
        RightSideThread,

        Diameter,
        Length,
        ChannelDiameter,
        WallThickness,

        KeyLength,
        KeySize,
        KeyOffset,

        StairDiameter,
        StairLength
    }

}
