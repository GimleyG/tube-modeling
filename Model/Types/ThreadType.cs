﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KompasLibrary.Model.Types
{
    /// <summary>
    /// Резьбовое соединение
    /// </summary>
    internal struct ThreadType
    {
        /// <summary>
        /// Название типа
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Наружный диаметр большего основания конуса
        /// </summary>
        public double Dl { get; set; }

        /// <summary>
        /// Наружный диаметр малого основания конуса
        /// </summary>
        public double Ds { get; set; }

        /// <summary>
        /// Конусность
        /// </summary>
        public double K { get; set; }

        /// <summary>
        /// Длина внешнего конуса
        /// </summary>
        public double Lpc { get; set; }

        /// <summary>
        /// Диаметр конусной выточки в плоскости упорного торца
        /// </summary>
        public double Qc { get; set; }

        /// <summary>
        /// Диаметр конусной выточки под резьбу
        /// </summary>
        public double Qs { get; set; }

        /// <summary>
        /// Длина внутреннего конуса
        /// </summary>
        public double Lbc { get; set; }

        /// <summary>
        /// Длина внутренней резьбы
        /// </summary>
        public double L { get; set; }

        /// <summary>
        /// Диаметр упорного торца
        /// </summary>
        public double Dt { get; set; }

        /// <summary>
        /// Смещение от торца до резьбового соединения (15.88)
        /// </summary>
        public double Ofs { get; set; }
    }
}
