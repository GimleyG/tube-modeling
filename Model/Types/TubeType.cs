﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KompasLibrary.Model.Types
{
    /// <summary>
    /// Названия типов трубы
    /// </summary>
    public enum TubeType
    {
        Standart,
        ConnectorNip,
        ConnectorMuf
    }
}
