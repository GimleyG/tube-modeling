﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KompasLibrary.Model.Types
{
    /// <summary>
    /// Структура для хранения диапазона значений
    /// </summary>
    public struct ValueRange
    {
        /// <summary>
        /// Минимальное значение диапазона
        /// </summary>
        public double Min { get; set; }

        /// <summary>
        /// Максимальное значение диапазона
        /// </summary>
        public double Max { get; set; }
    }
}
